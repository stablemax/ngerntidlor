﻿<%@ taglib prefix = "c" uri = "http://java.sun.com/jsp/jstl/core" %>
<html>

<head>
    <div w3-include-html="../include/link.jsp"></div>
    <title>Show calendar</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
        integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
        integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo"
        crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"
        integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1"
        crossorigin="anonymous"></script>
    <!-- <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"
        integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM"
        crossorigin="anonymous"></script> -->
    <link rel="stylesheet" type="text/css" href="css/styles.css">
    <style>
        @media (max-width: 420px) {
            .form-register {
                width: 90% !important;
            }



            .pad-top {
                padding-top: 20px;
            }

            .footer {
                position: fixed;
                left: 0;
                bottom: 0;
                width: 100%;
                color: white;
                text-align: center;
            }

            .img-wid {
                width: 90%;
            }
        }

        /* @media (width: 320px) and (height: 568px) {
            .form-register {}
        } */

        @media only screen and (min-width: 600px) {
            .form-register {
                width: 90% !important;

            }

            .pad-top {
                padding-top: 20px;
            }

            .footer {
                position: fixed;
                left: 0;
                bottom: 0;
                width: 100%;

                color: white;
                text-align: center;
            }
        }

        /* Large devices (laptops/desktops, 992px and up) */
        @media only screen and (min-width: 992px) {
            .form-register {
                width: 500px;
            }

            .footer {
                text-align: center;
                position: fixed;
            }

            .pad-top {
                padding-top: 20px;
            }

            .img-wid {
                width: 1000px;
            }
        }

    </style>
    <%@ include file="navbarInclude.jsp" %>
</head>
<%@ include file="headCustomer.jsp" %>


<body style="font-size: 13px">
    <br>
    <div class="container">
        <div class="row">
            <div class="col-12">


                <form class="form-check border-shadow form-register" action="showcalendar" method="post">
                    <span class="p-t-20 p-b-45">
                        <h3 style="padding: 10px; text-align:center">
                            ปฏิทิน
                        </h3>
                    </span>

                    <div class="form-register" style="text-align: center;">

                        <label>เดือน:</label>

                        <input type="month" id="month" name="month" min="2019-03" value="${month}">

                        <button class="myBtn" style="width: 30%" onclick="form1.submit();">
                            ค้นหา
                        </button>

                    </div>

                </form>
            </div>
        </div>
    </div>

    <br>

    <c:if test="${month!=null}">
        <div class="container border-shadow form-register" style="text-align: center;">

            <div class="row">
                <div class="col-12">
                    <p>${month}</p>
                </div>
                <div style="padding:5px; color:red;font-style:italic;" class="col-12">
                    <c:if test="${pics[0]==''}">
                        <p>ไม่พบปฏิทิน</p>
                    </c:if>
                </div>
                <div class="col-12">
                    <c:if test="${pics[0]!=''}"><img class="img-wid" src="${pics[0]}">
                    </c:if>

                </div>
            </div>

            <br>

        </div>
    </c:if>

    </div>
    <br>

    <%@ include file="footer.jsp" %>


</body>

</html>
