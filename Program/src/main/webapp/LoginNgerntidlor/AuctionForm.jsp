﻿<%@ taglib prefix = "c" uri = "http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html lang="en">

<head>
    <div w3-include-html="../include/link.jsp"></div>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <style>
        @import url('https://fonts.googleapis.com/css?family=Kanit&display=swap');

        .form-auc {
            padding: 30px 0;
            width: 100%;
            margin: auto;
            border-radius: 10px;
        }

    </style>

    <link rel="stylesheet" type="text/css" href="css/styles.css">
    <%@ include file="navbarInclude.jsp" %>
</head>
<%@ include file="headCustomer.jsp" %>

<body>

    <form id="form1" action="auctionsave" method="post" onsubmit="return saveform();">
        <div class="form-register" style="width:90%; ">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12 form-auc border-shadow ">
                        <img src="${pics}" style="width: 100%">
                    </div>
                    <div class="col-md-6">
                        <br>
                        <div class="col-12 form-auc border-shadow">
                            <h5 style="text-align:center">
                                <c:out value="${vehicle.getVehicleBrand()}" />
                                <c:out value="${vehicle.getVehicleModel()}" />
                            </h5>
                        </div>
                        <br>

                        <div class="col-12 form-auc border-shadow">

                            <h4>ข้อมูลรถ</h4>
                            <div class="row" style="justify-content: space-between">
                                <div style="padding-left: 15px">เลขที่สัญญา</div>
                                <div style="padding-right: 15px">
                                    <c:out value="${vehicle.getAgreementNo()}" />
                                </div>
                            </div>
                            <div class="row" style="justify-content: space-between">
                                <div style="padding-left: 15px">วันที่ยึด</div>
                                <div style="padding-right: 15px">
                                    <c:out value="${vehicle.getRepodate()}" />
                                </div>
                            </div>
                            <div class="row" style="justify-content: space-between">
                                <div style="padding-left: 15px">สถานะข้อมูล</div>
                                <div style="padding-right: 15px">
                                    <c:out value="${vehicle.getProcessDescThai()}" />
                                </div>
                            </div>
                            <div class="row" style="justify-content: space-between">
                                <div style="padding-left: 15px">เลขที่เครื่องยนต์</div>
                                <div style="padding-right: 15px">
                                    <c:out value="${vehicle.getAUTO_ENGINE_NO()}" />
                                </div>
                            </div>
                            <div class="row" style="justify-content: space-between">
                                <div style="padding-left: 15px">เลขที่ตัวถัง</div>
                                <div style="padding-right: 15px">
                                    <c:out value="${vehicle.getAUTO_BODY_NO()}" />
                                </div>
                            </div>
                            <div class="row" style="justify-content: space-between">
                                <div style="padding-left: 15px">เชื้อเพลิง</div>
                                <div style="padding-right: 15px">
                                    <c:out value="${vehicle.getENGINE_TYPE_DESC()}" />
                                </div>
                            </div>
                            <div class="row" style="justify-content: space-between">
                                <div style="padding-left: 15px">Mile(km.)</div>
                                <div style="padding-right: 15px">
                                    <c:out value="${vehicle.getPhysicalMile()}" />
                                </div>
                            </div>
                            <div class="row" style="justify-content: space-between">
                                <div style="padding-left: 15px">สี</div>
                                <div style="padding-right: 15px">
                                    <c:out value="${vehicle.getVehicleColor()}" />
                                </div>
                            </div>
                            <div class="row" style="justify-content: space-between">
                                <div style="padding-left: 15px">ทะเบียน</div>
                                <div style="padding-right: 15px">
                                    <c:out value="${vehicle.getRegisteration()}" />
                                </div>
                            </div>
                            <div class="row" style="justify-content: space-between">
                                <div style="padding-left: 15px">จังหวัดทะเบียน</div>
                                <div style="padding-right: 15px">
                                    <c:out value="${vehicle.getRegisterationProvince()}" />
                                </div>
                            </div>
                        </div>
                        <br>

                        <input type="hidden" name="vehicleid" value="${vehicle.getID()}"><input type="hidden"
                            name="auctionid" value="${auction.getId()}">


                    </div>
                    <div class="col-md-6">
                        <br>

                        <div class="form-auc border-shadow">
                            <div class="form-input">
                                ราคาเปิดประมูล :
                                <input class="register-input" type="text" name="pricestart" id="pricestart"
                                    value="${auction.getPriceauction()}" disabled>
                            </div>
                            <div class="form-input">
                                ราคาที่ต้องการประมูล :
                                <input class="register-input" type="number" name="priceauction" id="priceauction"
                                    placeholder="ราคา">
                                <button type="submit" class="btn-primary">เสนอราคา</button>
                            </div>
                        </div>
                        <br>
                        <div class="form-auc border-shadow" style="text-align: center">
                            มีผู้เช้าชม
                            <c:out value="${counter}" /> คน
                            <br><br>
                            มีผู้ประมูล
                            <c:out value="${countauction}" /> คน
                        </div>
                    </div>
                </div>
            </div>
        </div>
        </div>



    </form>

    <script>
        function saveform() {
            if ($("#priceauction").val() == 0) {
                alert("กรุณากรอกยอดประมูล");
                $("#priceauction").focus();
                return false;
            }
            if ($("#priceauction").val() >= 100000) {
                alert("ยอดการประมูลจะไม่เกิน 100000 บาท");
                $("#priceauction").focus();
                return false;
            }
            if ($("#priceauction").val() <= $("#pricestart").val()) {
                alert("ยอดการประมูลห้ามต่ำกว่า " + $("#pricestart").val() + " บาท");
                $("#priceauction").focus();
                return false;
            }

            if (confirm("กรุณาตรวจสอบความถูกต้องยอดประมูลของท่านคือ" + $("#priceauction").val())) {
                return true;
            } else {
                $("#priceauction").focus();
                return false;
            }
        }
    </script>


</body>

</html>
