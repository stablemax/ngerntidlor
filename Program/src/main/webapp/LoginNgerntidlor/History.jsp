﻿<%@ taglib prefix = "c" uri = "http://java.sun.com/jsp/jstl/core" %>

<!DOCTYPE html>
<html>

<head>

  <div w3-include-html="../include/link.jsp"></div>
  <title>รายการประมูลทั้งหมด</title>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">


  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
    integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
  <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
    integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo"
    crossorigin="anonymous"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"
    integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1"
    crossorigin="anonymous"></script>
  <!-- <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"
    integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM"
    crossorigin="anonymous"></script> -->
  <link rel="stylesheet" type="text/css" href="css/styles.css">

  <style>
    * {
      margin: 0px;
      padding: 0px;
      box-sizing: border-box;
    }

    body,
    html {
      height: 100%;
      font-family: sans-serif;
    }

    /* ------------------------------------ */
    a {
      margin: 0px;
      transition: all 0.4s;
      -webkit-transition: all 0.4s;
      -o-transition: all 0.4s;
      -moz-transition: all 0.4s;
    }


.footer {
                position: fixed;
                left: 0;
                bottom: 0;
                width: 100%;
               
                color: white;
                text-align: center;
            }


    /* ------------------------------------ */
    h1,
    h2,
    h3,
    h4,
    h5,
    h6 {
      margin: 0px;
    }

    p {
      margin: 0px;
    }

    ul,
    li {
      margin: 0px;
      list-style-type: none;
    }


    /* ------------------------------------ */
    input {
      display: block;
      outline: none;
      border: none !important;
    }

    textarea {
      display: block;
      outline: none;
    }



    /* ------------------------------------ */
    button {
      outline: none !important;
      border: none;
      background: transparent;
    }






    .limiter {
      width: 100%;
      margin: 0 auto;
    }

    .container-table100 {
      width: 100%;

      display: -webkit-box;
      display: -webkit-flex;
      display: -moz-box;
      display: -ms-flexbox;
      display: flex;
      align-items: center;
      justify-content: center;
      flex-wrap: wrap;
      padding: 33px 30px;
    }

    .wrap-table100 {
      width: 1170px;
    }

    table {
      border-spacing: 1;
      border-collapse: collapse;
      background: white;
      border-radius: 10px;
      overflow: hidden;
      width: 100%;
      margin: 0 auto;
      position: relative;
    }

    table * {
      position: relative;
    }

    table td,
    table th {
      padding-left: 8px;
    }

    table thead tr {
      height: 60px;
      background: #0060ab;
    }

    table tbody tr {
      height: 50px;
    }

    table tbody tr:last-child {
      border: 0;
    }

    table td,
    table th {
      text-align: left;
    }

    table td.l,
    table th.l {
      text-align: right;
    }

    table td.c,
    table th.c {
      text-align: center;
    }

    table td.r,
    table th.r {
      text-align: center;
    }


    .table100-head th {
      font-family: OpenSans-Regular;
      font-size: 18px;
      color: #fff;
      line-height: 1.2;
      font-weight: unset;
    }

    tbody tr:nth-child(even) {
      background-color: #f5f5f5;
    }

    tbody tr {
      font-family: OpenSans-Regular;
      font-size: 15px;
      color: #808080;
      line-height: 1.2;
      font-weight: unset;
    }

    tbody tr:hover {
      color: #555555;
      background-color: #f5f5f5;
      cursor: pointer;
    }

    .column1 {
      width: 10%;
      padding-left: 40px;
    }

    .column2 {
      width: 10%;
    }

    .column3 {
      width: 245px;
    }

    .column4 {
      width: 210px;
      /* text-align: right; */
    }

    .column5 {
      width: 170px;
      /* text-align: right; */
    }

    .column6 {
      width: 172px;
      /* text-align: right; */
      padding-right: 62px;
    }

    .column7 {
      width: 172px;
      text-align: right;
      padding-right: 62px;
    }

    .column8 {
      width: 172px;
      text-align: right;
      padding-right: 62px;
    }




    @media screen and (max-width: 992px) {
      table {
        display: block;
      }

      table>*,
      table tr,
      table td,
      table th {
        display: block;
      }

      table thead {
        display: none;
      }

      table tbody tr {
        height: auto;
        padding: 37px 0;
      }

      table tbody tr td {
        padding-left: 40% !important;
        margin-bottom: 24px;
      }

      table tbody tr td:last-child {
        margin-bottom: 0;
      }

      table tbody tr td:before {
        font-family: OpenSans-Regular;
        font-size: 14px;
        color: #999999;
        line-height: 1.2;
        font-weight: unset;
        position: absolute;
        width: 40%;
        left: 30px;
        top: 0;
      }

      table tbody tr td:nth-child(1):before {
        content: "ลำดับตัวเลข";
      }

      table tbody tr td:nth-child(2):before {
        content: "ยี่ห้อ";
      }

      table tbody tr td:nth-child(3):before {
        content: "รุ่น";
      }

      table tbody tr td:nth-child(4):before {
        content: "เลขตัวรถ";
      }

      table tbody tr td:nth-child(5):before {
        content: "ทะเบียน";
      }

      table tbody tr td:nth-child(6):before {
        content: "ทะเบียนจังหวัด";
      }

      table tbody tr td:nth-child(7):before {
        content: "ราคาประมูล";
      }

      table tbody tr td:nth-child(8):before {
        content: "สถานะ";
      }

      .column4,
      .column5,
      .column6 {
        text-align: left;
      }

      .column4,
      .column5,
      .column6,
      .column1,
      .column2,
      .column3 {
        width: 100%;
      }

      tbody tr {
        font-size: 14px;
      }
    }

    @media (max-width: 576px) {
      .container-table100 {
        padding-left: 15px;
        padding-right: 15px;
      }
    }

    @media screen and (max-width: 600px) {
      #title_message {
        visibility: hidden;
        clear: both;
        float: left;
        margin: 10px auto 5px 20px;
        width: 28%;
        display: none;
      }
    }

  </style>

  <%@ include file="navbarInclude.jsp" %>
</head>
<%@ include file="headCustomer.jsp" %>

<body>



  <div class="limiter ">
    <div class="container-table100">
      <div class="wrap-table100 border-shadow">
        <div class="table100">
          <table>
            <thead>
              <tr class="table100-head" style="text-align: center">
                <th class="column1">ลำดับตัวเลข</th>
                <th class="column2">ยี่ห้อ</th>
                <th class="column3">รุ่น</th>
                <th class="column4">เลขตัวรถ</th>
                <th class="column5">ทะเบียน</th>
                <th class="column6">ทะเบียนจังหวัด</th>
                <th class="column6">ราคาประมูล</th>
                <th class="column6">สถานะ</th>

              </tr>
            </thead>
            <tbody>
              <c:forEach var="history" items="${histories}" begin="0" varStatus="Index">
                <tr>
                  <td class="column1">
                    <c:out value="${Index.count}" />
                  </td>
                  <td class="column2">
                    <c:out value="${history.getVehiclebrand()}" />
                  </td>
                  <td class="column3">
                    <c:out value="${history.getVehiclemodel()}" />
                  </td>
                  <td class="column4">
                    <c:out value="${history.getAutobodyno()}" />
                  </td>
                  <td class="column5">
                    <c:out value="${history.getRegisteration()}" />
                  </td>
                  <td class="column6">
                    <c:out value="${history.getRegisterationprovince()}" />
                  </td>
                  <td class="column5">
                    <c:out value="${history.getStatuswin()}" />
                  </td>
                  <td class="column6">
                    <c:out value="${history.getStatusauction()}" />
                  </td>
                </tr>

              </c:forEach>

            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>

 

  <%@ include file="footer.jsp" %>


</body>

</html>
