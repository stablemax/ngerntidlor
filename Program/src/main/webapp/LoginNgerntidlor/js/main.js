
(function ($) {
    "use strict";

    /*==================================================================
    [ Validate ]*/
    var input = $('.validate-input .input100');

    $('.validate-form').on('submit', function () {
        var check = true;

        for (var i = 0; i < input.length; i++) {
            if (validate(input[i]) == false) {
                showValidate(input[i]);
                check = false;
            }
        }
        // password must be 6-12 digit

        if ($('#password').val().length <= 5 || $('#password').val().length >= 13) {
            showCfValidate('#password');
            check = false;

        } else {
            hideCfValidate('#password');
        }

        // password and confirm password must be match
        if ($('#password').val() != $('#cfpassword').val()) {
            showCfValidate('#cfpassword');
            check = false;

        } else {
            hideCfValidate('#cfpassword');
        }

        // id must be 13 digit
        if ($('#idcard').val().length != 13) {
            showCfValidate('#idcard');
            check = false;

        } else {
            hideCfValidate('#idcard');
        }


        // user must be 6-12 digit
        if ($('#username').val().length <= 5 || $('#username').val().length >= 13) {
            showCfValidate('#username');
            check = false;

        } else {
            hideUserCheckValidate('#username');
        }

        // user must not been taken
        // if (!check3) {
        //     showUserCheckValidate('#username');
        //     check = false;

        // } else {
        //     hideCfValidate('#username');
        // }

        // return check;
    });


    $('.validate-form .input100').each(function () {
        $(this).focus(function () {
            hideValidate(this);
        });
    });

    function validate(input) {


        if ($(input).attr('type') == 'email' || $(input).attr('name') == 'email') {
            if ($(input).val().trim().match(/^([a-zA-Z0-9_\-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\.)+))([a-zA-Z]{1,5}|[0-9]{1,3})(\]?)$/) == null) {
                return false;
            }
        }
        else
            if ($(input).val().trim() == '') {
                return false;
            }

    }

    function showValidate(input) {
        var thisAlert = $(input).parent();

        $(thisAlert).addClass('alert-validate');
    }

    function hideValidate(input) {
        var thisAlert = $(input).parent();

        $(thisAlert).removeClass('alert-validate');
    }

    function showCfValidate(input) {
        var thisAlert = $(input).parent();

        $(thisAlert).addClass('cf-validate');
    }

    function hideCfValidate(input) {
        var thisAlert = $(input).parent();

        $(thisAlert).removeClass('cf-validate');
    }

    // function showUserCheckValidate(input) {
    //     var thisAlert = $(input).parent();

    //     $(thisAlert).addClass('userCheck-validate');
    // }

    // function hideUserCheckValidate(input) {
    //     var thisAlert = $(input).parent();

    //     $(thisAlert).removeClass('userCheck-validate');
    // }




})(jQuery);