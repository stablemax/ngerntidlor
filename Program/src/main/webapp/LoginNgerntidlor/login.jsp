﻿<%@ taglib prefix = "c" uri = "http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html lang="en">
<div w3-include-html="../include/link.jsp"></div>

<head>
	<style>
		@media only screen and (min-width: 600px) {
		
body{
	padding-top: 200px;
}
			#form1 {
				font-size: 30px;

			}
			.form-register{
			
			
			
			}
			.register-input{

			}

		}

		/* Large devices (laptops/desktops, 992px and up) */
		@media only screen and (min-width: 992px) {
			body {
				padding: 10px;
			}

			#form1 {
				font-size: 16px;
				
			}
			.form-register{
			width: 500px;
			}
		}

		.footer {
			position: fixed;
			left: 0;
			bottom: 0;
			width: 100%;
			
			color: white;
			text-align: center;
		}

		@import url('https://fonts.googleapis.com/css?family=Kanit&display=swap');

	</style>
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
		integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
	<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
		integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo"
		crossorigin="anonymous"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"
		integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1"
		crossorigin="anonymous"></script>
	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"
		integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM"
		crossorigin="anonymous"></script>
	<link rel="stylesheet" type="text/css" href="css/styles.css">
	<%@ include file="navbarInclude.jsp" %>
</head>

<body>

	<div class="container">
		<div class="row">
			<div class="col-12">
				<div style="padding: 30px">

				</div>
				<div id="form1" class="form-register border-shadow">
					<form name="form1" action="login" method="post" >
						<div style="text-align: center;">
							<img src="LoginNgerntidlor/images/Logo-NTL-no SS-01.jpg" class="logo-circle">
						</div>
						<div class="form-input">
							ชื่อผู้ใช้:
							<input class="register-input" type="text" name="username" placeholder="">
						</div>
						<div class="form-input">
							รหัสผ่าน:
							<input class="register-input" type="password" name="password" placeholder="">
						</div>

						<div class="btn-center">
							<button class="myBtn" onclick="form1.submit();">
								Login
							</button>
						</div>



						<div class="btn-center">
							<p style="color:red;">
								<c:out value="${output}" />
							</p>
							<a class="txt1" href="register">
								ลงทะเบียน
							</a>



						</div>





					</form>
				</div>
			</div>
		</div>
	</div>


				

				<%@ include file="footer.jsp" %>

				<!--===============================================================================================-->
				<script src="LoginNgerntidlor/vendor/jquery/jquery-3.2.1.min.js"></script>
				<!--===============================================================================================-->
				<script src="LoginNgerntidlor/vendor/bootstrap/js/popper.js"></script>
				<script src="LoginNgerntidlorvendor/bootstrap/js/bootstrap.min.js"></script>
				<!--===============================================================================================-->
				<script src="LoginNgerntidlorvendor/select2/select2.min.js"></script>
				<!--===============================================================================================-->
				<script src="LoginNgerntidlor/js/main.js"></script>

</body>

</html>
