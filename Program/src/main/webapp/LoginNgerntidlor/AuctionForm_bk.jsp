<!DOCTYPE html>
<html lang="en">

<head>
    <style>
        @import url('https://fonts.googleapis.com/css?family=Kanit&display=swap');

        .form-auc {
            padding: 30px 0;
            width: 100%;
            margin: auto;
            border-radius: 10px;
        }

    </style>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
        integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
        integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo"
        crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"
        integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1"
        crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"
        integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM"
        crossorigin="anonymous"></script>
    <link rel="stylesheet" type="text/css" href="/css/styles.css">
</head>

<body>

    <div class="form-register">
        <div class="container">
            <div class="row">
                <div class="col-6">
                    <div>
                        <img src="https://www.checkraka.com/uploaded/_resized/width_700x467/cf/cfd39cb341d310ce100995a95961bac3.png"
                            style="width: 100%">
                    </div>
                    <br>
                    <div class="form-auc border-shadow">
                        <div class="form-input">
                            ราคาเปิดประมูล :
                            <input class="register-input" type="text" name="firstname" id="firstname"
                                placeholder="25000">
                        </div>
                        <div class="form-input">
                            ราคาที่ต้องการประมูล :
                            <input class="register-input" type="text" name="firstname" id="firstname"
                                placeholder="กรุณาใส่ราคาที่ต้องการประมูล">
                        </div>
                    </div>
                    <br>
                    <div class="form-auc border-shadow" style="text-align: center">
                        มีผู้เช้าชม_คน
                    </div>
                    <br>
                    <div class="form-auc border-shadow" style="text-align: center">
                        มีผู้ประมูล_คน
                    </div>

                </div>
                <div class="col-6 form-auc border-shadow">
                    <div style="padding:20px">
                        <h4>ข้อมูลรถ</h4>
                        <div class="row" style="justify-content: space-between">
                            <div class="">เลขที่สัญญา</div>
                            <div class="">input here!</div>
                        </div>
                        <div class="row" style="justify-content: space-between">
                            <div class="">วันที่ยึด</div>
                            <div class="">input here!</div>
                        </div>
                        <div class="row" style="justify-content: space-between">
                            <div class="">สถานะข้อมูล</div>
                            <div class="">input here!</div>
                        </div>
                        <div class="row" style="justify-content: space-between">
                            <div class="">เลขที่เครื่องยนต์</div>
                            <div class="">input here!</div>
                        </div>
                        <div class="row" style="justify-content: space-between">
                            <div class="">เลขที่ตัวถัง</div>
                            <div class="">input here!</div>
                        </div>
                        <div class="row" style="justify-content: space-between">
                            <div class="">เชื้อเพลิง</div>
                            <div class="">input here!</div>
                        </div>
                        <div class="row" style="justify-content: space-between">
                            <div class="">Mile(km.)</div>
                            <div class="">input here!</div>
                        </div>
                        <div class="row" style="justify-content: space-between">
                            <div class="">สี</div>
                            <div class="">input here!</div>
                        </div>
                        <div class="row" style="justify-content: space-between">
                            <div class="">ทะเบียน</div>
                            <div class="">input here!</div>
                        </div>
                        <div class="row" style="justify-content: space-between">
                            <div class="">จังหวัดทะเบียน</div>
                            <div class="">input here!</div>
                        </div>
                    </div>

                </div>
            </div>

        </div>

    </div>






    < </body> </html>
