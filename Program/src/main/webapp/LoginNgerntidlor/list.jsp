﻿<%@ taglib prefix = "c" uri = "http://java.sun.com/jsp/jstl/core" %>

<!DOCTYPE html>
<html lang="en">

<head>
    <div w3-include-html="../include/link.jsp"></div>
    <title>Vehicle List</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="stylesheet" type="text/css" href="/css/styles.css">
    <%@ include file="navbarInclude.jsp" %>
</head>
<%@ include file="headCustomer.jsp" %>

<body>

    <div class="container">
        <div class="row">
            <div class="col-12 limiter">
                <div class="wrap-login100  ">
                    <form name="form1" class="form-register " action="list" method="get">
                        <!-- <input type="hidden" name="page" value="${page}"> -->
                        <span class="login100-form-title p-t-20 p-b-45">
                            <h1>
                                แคตตาล็อก
                            </h1>
                        </span>
                        <span class="login100-form-title p-t-20 p-b-20">
                            ค้นหา
                        </span>
                        <!-- <div class="wrap-input100 validate-input m-b-10" data-validate="Username is required">

                        <input class="input100" type="text" name="registeration" placeholder="เลขทะเบียน"
                            value="${s_registeration}" class="form-control">
                        <span class="focus-input100"></span>
                        <span class="symbol-input100">
                            <i class="fa fa-search"></i>
                        </span>
                    </div> -->
                        <div class="wrap-input100 validate-input m-b-10" data-validate="Username is required">
                            <select class="form-control" name="brand">
                                <option value="">เลือก</option>
                                <c:forEach var="brand" items="${brands}">
                                    <option value="${brand}" <c:if test="${brand==s_brand}">selected
                                        </c:if> >
                                        <c:out value="${brand}" />
                                    </option>
                                </c:forEach>
                            </select>
                        </div>
                        <div class="wrap-input100 validate-input m-b-10" data-validate="Username is required">
                            <select class="form-control" name="veheclemodel">
                                <option value="">เลือก</option>
                                <c:forEach var="vehecle_model" items="${vehecle_models}">
                                    <option value="${vehecle_model}" <c:if test="${vehecle_model==s_vehecle_model}">
                                        selected
                                        </c:if>>
                                        <c:out value="${vehecle_model}" />
                                    </option>
                                </c:forEach>
                            </select>
                        </div>
                        <div class="wrap-input100 validate-input m-b-10" data-validate="Username is required">
                            <select class="form-control" name="year">
                                <option value="">เลือก</option>
                                <c:forEach var="year" items="${years}">
                                    <option value="${year}" <c:if test="${year==s_year}">selected
                                        </c:if> >
                                        <c:out value="${year}" />
                                    </option>
                                </c:forEach>
                            </select>
                        </div>


                        <div class="container-login100-form-btn p-t-10">
                            <button class="login100-form-btn btn btn-primary" onclick="form1.submit();">
                                ค้นหา
                            </button>
                        </div>

                    </form>
                </div>

                <form name="form1" class="login100-form validate-form " action="list" method="post">



                    <div class="container  ">
                        <div class="row">
                            <c:forEach var="vehicle" items="${vehicles}">
                                <div class="col-md-6 col-lg-4 col-xl-3  border-shadow">

                                    </br>
                                    <div class="container" style="background-color: white ; ">
                                        <div class="p-t-50 p-b-50">
                                            <img src="uploadDir/${vehicle.getID()}.1.jpg"
                                                onerror="this.onerror=null; this.src='LoginNgerntidlor/images/scooter.svg'"
                                                width="100%" height="100%">
                                        </div>
                                        <!-- เลขที่สัญญา:
                                        <c:out value="${vehicle.getAgreementNo()}" />
                                        </br> -->

                                        รุ่นรถ:
                                        <c:out value="${vehicle.getVehicleModel()}" />
                                        </br>

                                        ปีรถ:
                                        <c:out value="${vehicle.getVehicleYear()}" />
                                        </br>

                                        เลขทะเบียน:
                                        <c:out value="${vehicle.getRegisteration()}" />
                                        </br>

                                        เลขเครื่องยนต์:
                                        <c:out value="${vehicle.getAUTO_ENGINE_NO()}" />
                                        </br>

                                        หมายเลขตัวถัง:
                                        <c:out value="${vehicle.getAUTO_BODY_NO()}" />

                                        <div class="row p-b-50">
                                            <div class="col"><button type="button"
                                                    onclick="window.location.href='catalog?id=${vehicle.getID()}'"
                                                    class="btn btn-primary">รายละเอียด</button>
                                            </div>
                                        </div>

                                    </div>
                                    <br />
                                </div>
                            </c:forEach>

                        </div>
                    </div>

                    <!-- <div class="container">
                        <div class="row">
                            <div class="form-register btn-center col-12">
                                <button type="button" class="btn btn-primary"
                                    onclick="window.location.href='list?page=${page-1}&registeration=${s_registeration}&brand=${s_brand}&veheclemodel=${s_vehecle_model}&year=${s_year}'"
                                    <c:if test="${page==1}">disabled
                                    </c:if>
                                    >Previous</button>
                                <c:forEach var="i" begin="${pagestart}" end="${pageend}" step="1" varStatus="status">
                                    &nbsp;
                                    <c:if test="${i==page}"><b>
                                            <c:out value="${i}" /></b></c:if>
                                    <c:if test="${i!=page}">
                                        <a
                                            href='list?page=${i}&registeration=${s_registeration}&brand=${s_brand}&veheclemodel=${s_vehecle_model}&year=${s_year}'>
                                            <c:out value="${i}" /></a></c:if>
                                </c:forEach>
                                <button type="button" class="btn btn-primary"
                                    onclick="window.location.href='list?page=${page+1}&registeration=${s_registeration}&brand=${s_brand}&veheclemodel=${s_vehecle_model}&year=${s_year}'"
                                    <c:if test="${numpage==page}">disabled
                                    </c:if> >Next</button>
                                </br></br>


                            </div>
                        </div>
                    </div> -->

                    <!-- <div class="text-center w-full">
						<a class="txt1" href="#">
							ลงทะเบียน
							<i class="fa fa-long-arrow-right"></i>
						</a>
					</div> -->
                </form>

            </div>
        </div>
    </div>
    <br />

    <%@ include file="footer.jsp" %>
    </div>






    <!--===============================================================================================-->
    <!-- <script src="LoginNgerntidlor/vendor/jquery/jquery-3.2.1.min.js"></script> -->
    <!--===============================================================================================-->
    <!-- <script src="LoginNgerntidlor/vendor/bootstrap/js/popper.js"></script> -->
    <!-- <script src="LoginNgerntidlorvendor/bootstrap/js/bootstrap.min.js"></script> -->
    <!--===============================================================================================-->
    <!-- <script src="LoginNgerntidlorvendor/select2/select2.min.js"></script> -->
    <!--===============================================================================================-->
    <!-- <script src="LoginNgerntidlor/js/main.js"></script> -->

    <script>
        function previousPage() {
            console.log("previousPage")
        }
        function nextPage() {
            console.log("nextPage")
        }


    </script>

</body>

</html>