﻿<%@ taglib prefix = "c" uri = "http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html lang="en">

<head>
    <div w3-include-html="../include/link.jsp"></div>
    <title>แก้ไขข้อมูล</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <style>
        @media (max-width: 420px) {
            .form-register {
                width: 90%;
            }

            .pad-top {
                padding-top: 20px;
            }

            .footer {
                position: relative;
                left: 0;
                bottom: 0;
                width: 100%;
                color: white;
                text-align: center;
            }
        }

        @media (width: 320px) and (height: 568px) {
            .form-register {}
        }

        @media only screen and (min-width: 600px) {
            .form-register {
                width: 90%;

            }

            .pad-top {
                padding-top: 20px;
            }

            .footer {
                position: relative;
                left: 0;
                bottom: 0;
                width: 100%;

                color: white;
                text-align: center;
            }
        }

        /* Large devices (laptops/desktops, 992px and up) */
        @media only screen and (min-width: 992px) {
            .form-register {
                width: 500px;
            }

            .footer {
                text-align: center;
                position: relative;
            }
        }

    </style>
    <script language="javascript">
        function checkID(id) {
            if (id.length != 13) return false;
            for (i = 0, sum = 0; i < 12; i++)
                sum += parseFloat(id.charAt(i)) * (13 - i); if ((11 - sum % 11) % 10 != parseFloat(id.charAt(12)))
                return false; return true;
        }

        function checkForm() {
            if (!checkID($("#idcard").val())) {
                $("#idcard").focus();
                alert('รหัสประชาชนไม่ถูกต้อง');
                return false;
            }
            if ($("#password").val() != $("#cfpassword").val()) {
                $("#cfpass").html("กรุณาใส่รหัสผ่านให้ตรงกัน")
                return false;

            }
        }

        function toggleReset() {
            if ($("#divReset").is(":hidden")) {
                $("#divReset").show();
            } else {
                $("#divReset").hide();
            }
        }
    </script>
    <link rel="stylesheet" type="text/css" href="css/styles.css">
    <%@ include file="navbarInclude.jsp" %>
</head>
<%@ include file="headCustomer.jsp" %>

<body>
    <div class="container">
        <div class="row">


            <div class="col-12" style="padding: 20px">

                <form name="form1" onsubmit="return checkForm();" action="profile" method="post">
            </div>
            <div class="form-register border-shadow">
                <div class="form-input">
                    ชื่อจริง:
                    <input class="register-input" type="text" name="firstname" id="firstname" required
                        placeholder="กรุณากรอกชื่อ" value="${user.firstname}"
                        oninvalid="this.setCustomValidity('กรุณากรอกชื่อจริง')" oninput="this.setCustomValidity('')">
                </div>
                <div class="form-input">
                    นามสกุล:
                    <input class="register-input" type="text" name="lastname" id="lastname" required
                        placeholder="กรุณากรอกนามสกุล" value="${user.lastname}"
                        oninvalid="this.setCustomValidity('กรุณากรอกนามสกุล')" oninput="this.setCustomValidity('')">
                </div>
                <div class="form-input">
                    รหัสประจำตัวประชาชน
                    <input class="register-input" type="number" name="idcard" id="idcard" required
                        placeholder="กรุณากรอกรหัสประจำตัวประชาชน 13 หลัก" value="${user.idcard}"
                        oninvalid="this.setCustomValidity('กรุณากรอกรหัสประจำตัวประชาชน')"
                        oninput="this.setCustomValidity('')">
                </div>
                <div class="form-input">
                    เบอร์โทรศัพท์
                    <input class="register-input" type="number" name="tel" id="tel" required
                        placeholder="กรุณากรอกเบอร์โทรศัพท์" value="${user.tel}"
                        oninvalid="this.setCustomValidity('กรุณากรอกเบอร์โทรศัพท์')"
                        oninput="this.setCustomValidity('')">
                </div>
                <div class="form-input">
                    อีเมลล์
                    <input class="register-input" type="email" name="email" required placeholder="กรุณากรอกอีเมลล์"
                        value="${user.email}" oninvalid="this.setCustomValidity('กรุณากรอกอีเมลล์ให้ถูกต้อง')"
                        oninput="this.setCustomValidity('')">
                </div>
                <div class="form-input">
                    ที่อยู่
                    <input class="register-input" type="text" name="address" required placeholder="กรุณากรอกที่อยู่"
                        value="${user.address}" oninvalid="this.setCustomValidity('กรุณากรอกที่อยู่')"
                        oninput="this.setCustomValidity('')">
                </div>
                <div class="form-input">
                    ไอดี Line
                    <input class="register-input" type="text" name="line" required placeholder="กรุณากรอกไอดี Line"
                        value="${user.line}" oninvalid="this.setCustomValidity('กรุณากรอกไอดี Line')"
                        oninput="this.setCustomValidity('')">
                </div>
                <div class="form-input">
                    ชื่อร้าน
                    <input class="register-input" type="text" name="storename" required placeholder="กรุณากรอกชื่อร้าน"
                        value="${user.storename}" oninvalid="this.setCustomValidity('กรุณากรอกชื่อร้าน')"
                        oninput="this.setCustomValidity('')">
                </div>

                <div class="form-input">
                    ชื่อผู้ใช้: <span>${user.username}</span>
                </div>
                <div class="btn-center">
                    <button onclick="toggleReset();return false;" class="myBtn">ตั้งรหัสผ่านใหม่</button>
                </div>
                <div id="divReset" style="display:none;">
                    <div class="form-input">
                        รหัสผ่านใหม่
                        <input class="register-input" type="password" name="password" id="password"
                            placeholder="รหัสผ่านใหม่" oninvalid="this.setCustomValidity('กรุณากรอกรหัสผ่าน')"
                            oninput="this.setCustomValidity('')">
                    </div>
                    <div class="form-input">
                        ยืนยันรหัสผ่านใหม่
                        <input class="register-input" type="password" name="cfpassword" id="cfpassword"
                            placeholder="ยืนยันรหัสผ่านใหม่"
                            oninvalid="this.setCustomValidity('กรุณากรอกรหัสผ่านอีกครั้ง')"
                            oninput="this.setCustomValidity('')">
                        <p id="cfpass" style="color:red;"></p>
                    </div>
                </div>

                <input type="hidden" name="username" value="${username}">

                <div class="btn-center">
                    <input type="submit" class="myBtn" value="ยืนยัน">
                </div>

                <form>
                    <div class="btn-center">
                        <input class="myBtn" type="button" value="กลับ" onclick="window.location.href='index'" />
                    </div>
                </form>

                </form>
            </div>
        </div>
    </div>

    <%@ include file="footer.jsp" %>


</body>

<script>
    var check = function () {
        if (document.getElementById('password').value ==
            document.getElementById('cfpassword').value) {
            document.getElementById('message').style.color = 'green';
            document.getElementById('message').innerHTML = 'matching';
        } else {
            document.getElementById('message').style.color = 'red';
            document.getElementById('message').innerHTML = 'not matching';
        }
    }
</script>

</html>
