﻿<%@ taglib prefix = "c" uri = "http://java.sun.com/jsp/jstl/core" %>
<html>

<head>
    <div w3-include-html="../include/link.jsp"></div>
    <title>Receipt</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="stylesheet" type="text/css" href="LoginNgerntidlor/css/util.css">
    <link rel="stylesheet" type="text/css" href="css/styles.css">
    <!--===============================================================================================-->

    <%@ include file="navbarInclude.jsp" %>
</head>
<%@ include file="headOff.jsp" %>

<body style="font-size: 13px">
    <br>
    <div class="container">
        <div class="row">
            <div class="col-12">


                <form class="form-check border-shadow" action="receipt" method="post">
                    <span class="p-t-20 p-b-45">
                        <h3 style="padding: 10px; text-align:center">
                            ใบเสร็จ
                        </h3>
                    </span>

                    <div class="m-b-10" style="text-align: center;" data-validate="Username is required">

                        <input class="register-input" style="width: 65%" type="text" name="keyword"
                            placeholder="ชื่อนามสกุล username หรือ ชื่อร้าน" value="${keyword}">

                        <button class="myBtn" style="width: 30%" onclick="form1.submit();">
                            ค้นหา
                        </button>
                    </div>
                </form>
            </div>
        </div>
    </div>

</body>
<!--===============================================================================================-->
<script src="LoginNgerntidlor/vendor/jquery/jquery-3.2.1.min.js"></script>
<!--===============================================================================================-->
<script src="LoginNgerntidlor/vendor/bootstrap/js/popper.js"></script>
<script src="/LoginNgerntidlorvendor/bootstrap/js/bootstrap.min.js"></script>
<!--===============================================================================================-->
<script src="/LoginNgerntidlorvendor/select2/select2.min.js"></script>
<!--===============================================================================================-->
<!-- <script src="LoginNgerntidlor/js/main.js"></script> -->


</html>