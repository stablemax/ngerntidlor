﻿<%@ taglib prefix = "c" uri = "http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<!DOCTYPE html>
<html lang="en">

<head>
    <div w3-include-html="../include/link.jsp"></div>
    <title>Vehicle List</title>
    <style>
        .img-1{
            width: 220px;
            height: 200px;
        }
    </style>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="stylesheet" type="text/css" href="/css/styles.css">
    <%@ include file="navbarInclude.jsp" %>
</head>
<%@ include file="headOff.jsp" %>

<body>

    <div class="container">
        <div class="row">
            <div class="col-12 limiter">
                <div class="wrap-login100  ">
                    <form name="form1" class="form-register " action="listoff" method="get">
                        <!-- <input type="hidden" name="page" value="${page}"> -->
                        <span class="login100-form-title p-t-20 p-b-45">
                            <h1 style="text-align: center">
                                แคตตาล็อก
                            </h1>
                        </span>
                        <span class="login100-form-title p-t-20 p-b-20">
                            ค้นหา
                        </span>

                        <div class="wrap-input100 validate-input m-b-10" data-validate="Username is required">
                            <select class="form-control" name="brand">
                                <option value="">เลือก</option>
                                <c:forEach var="brand" items="${brands}">
                                    <option value="${brand}" <c:if test="${brand==s_brand}">selected
                                        </c:if> >
                                        <c:out value="${brand}" />
                                    </option>
                                </c:forEach>
                            </select>
                        </div>
                        <div class="wrap-input100 validate-input m-b-10" data-validate="Username is required">
                            <select class="form-control" name="veheclemodel">
                                <option value="">เลือก</option>
                                <c:forEach var="vehecle_model" items="${vehecle_models}">
                                    <option value="${vehecle_model}" <c:if test="${vehecle_model==s_vehecle_model}">
                                        selected
                                        </c:if>>
                                        <c:out value="${vehecle_model}" />
                                    </option>
                                </c:forEach>
                            </select>
                        </div>
                        <div class="wrap-input100 validate-input m-b-10" data-validate="Username is required">
                            <select class="form-control" name="year">
                                <option value="">เลือก</option>
                                <c:forEach var="year" items="${years}">
                                    <option value="${year}" <c:if test="${year==s_year}">selected
                                        </c:if> >
                                        <c:out value="${year}" />
                                    </option>
                                </c:forEach>
                            </select>
                        </div>


                        <div class="container-login100-form-btn p-t-10">
                            <button class="login100-form-btn btn btn-primary" onclick="form1.submit();">
                                ค้นหา
                            </button>
                        </div>

                    </form>
                </div>

                <div class="container  ">
                    <div class="row">
                        <c:forEach var="vehicle" items="${vehicles}">
                            <div class="col-md-6 col-lg-4 col-xl-3  border-shadow">

                                </br>
                                <div class="container" style="background-color: white ; ">
                                    <div class="img-1">
                                        <img src="uploadDir/${vehicle.getID()}.1.jpg"
                                            onerror="this.onerror=null; this.src='LoginNgerntidlor/images/scooter.svg'"
                                            width="100%" height="100%">
                                    </div>
                                    เลขที่สัญญา:
                                    <c:out value="${vehicle.getAgreementNo()}" />
                                    </br>

                                    รุ่นรถ:
                                    <c:out value="${vehicle.getVehicleModel()}" />
                                    </br>

                                    ปีรถ:
                                    <c:out value="${vehicle.getVehicleYear()}" />
                                    </br>

                                    เลขทะเบียน:
                                    <c:out value="${vehicle.getRegisteration()}" />
                                    </br>

                                    เลขเครื่องยนต์:
                                    <c:out value="${vehicle.getAUTO_ENGINE_NO()}" />
                                    </br>

                                    หมายเลขตัวถัง:
                                    <c:out value="${vehicle.getAUTO_BODY_NO()}" />
                                    </br>

                                    <!-- ID:
                                    <c:out value="${vehicle.getID()}" />
                                    </br> -->

                                    ราคาสูงสุด:
                                    <fmt:formatNumber pattern="#,##0" value="${vehicle.getPricemax()}" />
                                    </br>
                                </div>
                                <br />
                            </div>
                        </c:forEach>

                    </div>
                </div>



            </div>
        </div>
    </div>
    <br />

    <%@ include file="footer.jsp" %>
    </div>






    <!--===============================================================================================-->
    <!-- <script src="LoginNgerntidlor/vendor/jquery/jquery-3.2.1.min.js"></script> -->
    <!--===============================================================================================-->
    <!-- <script src="LoginNgerntidlor/vendor/bootstrap/js/popper.js"></script> -->
    <!-- <script src="LoginNgerntidlorvendor/bootstrap/js/bootstrap.min.js"></script> -->
    <!--===============================================================================================-->
    <!-- <script src="LoginNgerntidlorvendor/select2/select2.min.js"></script> -->
    <!--===============================================================================================-->
    <!-- <script src="LoginNgerntidlor/js/main.js"></script> -->

    <script>
        function previousPage() {
            console.log("previousPage")
        }
        function nextPage() {
            console.log("nextPage")
        }


    </script>

</body>

</html>