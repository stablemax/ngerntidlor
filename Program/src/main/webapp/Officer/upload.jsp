﻿<%@ taglib prefix = "c" uri = "http://java.sun.com/jsp/jstl/core" %>
<html>

<head>
    <div w3-include-html="../include/link.jsp"></div>

    <title>upload</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="stylesheet" type="text/css" href="LoginNgerntidlor/css/util.css">
    <link rel="stylesheet" type="text/css" href="css/styles.css">
    <!--===============================================================================================-->
    <style>
        .footer {
            position: fixed;
            left: 0;
            bottom: 0;
            width: 100%;

            color: white;
            text-align: center;
        }

        #file {
            width: 100%;
            height: auto;
        }

        #img {
            width: 90%;
            height: auto;
        }

    </style>

    <%@ include file="navbarInclude.jsp" %>
</head>
<%@ include file="headOff.jsp" %>

<body style="font-size: 13px">
    <br>
    <div class="container">
        <div class="row">
            <div class="col-12">


                <form class="form-check border-shadow" action="upload" method="post">
                    <span class="p-t-20 p-b-45">
                        <h3 style="padding: 10px; text-align:center">
                            อัพโหลดรูปรถเข้าประมูลออนไลน์
                        </h3>
                    </span>

                    <div class="container">
                        <div class="row">
                            <div class="col-md-8">
                                <input class="register-input" type="text" name="keyword"
                                    placeholder="เลขที่สัญญา หรือ เลขทะเบียน" value="${keyword}">

                            </div>

                            <div class="col-md-4" style="margin: auto; text-align: center">
                                <button class="myBtn" onclick="form1.submit();" style="width: 100%">
                                    ค้นหา
                                </button>
                            </div>
                        </div>
                    </div>

                </form>
            </div>
        </div>
    </div>


    <c:if test="${vehicle.getVehicleBrand()!=null}">
        <br><br>
        <div class="container border-shadow" style="text-align: center">
            <div class="row col-left">
                <div class="col-4 col-border">
                    ยี่ห้อ
                </div>
                <div class="col-4 col-border">
                    รุ่น
                </div>
                <div class="col-4 col-border">
                    ทะเบียน
                </div>
                <div class="col-4 col-border">
                    <c:out value="${vehicle.getVehicleBrand()}" />
                </div>
                <div class="col-4 col-border">
                    <c:out value="${vehicle.getVehicleModel()}" />
                </div>
                <div class="col-4 col-border">
                    <c:out value="${vehicle.getRegisteration()}" />
                </div>
            </div>
        </div>

        <br><br>

        <div class="container border-shadow" style="text-align: center">
            <div class="row col-left">
                <div class="col-4 col-border">
                    หัวข้อ
                </div>
                <div class="col-4 col-border">
                    ข้อมูล
                </div>
                <div class="col-4 col-border">
                    ความถูกต้อง
                </div>
                <div class="col-4 col-border">
                    เลขที่สัญญา
                </div>
                <div class="col-4 col-border">
                    <c:out value="${vehicle.getAgreementNo()}" />
                </div>
                <div class="col-4 col-border checkbox-mid">
                    <c:choose>
                        <c:when test="${chkVehicle.getChk_AgreementNo() == '1'}">
                            ถูกต้อง
                        </c:when>
                        <c:when test="${chkVehicle.getChk_AgreementNo() == '0'}">
                            ไม่ถูกต้อง
                        </c:when>
                    </c:choose>
                </div>
                <div class="col-4 col-border">
                    เลขเครื่องยนต์
                </div>
                <div class="col-4 col-border">
                    <c:out value="${vehicle.getAUTO_ENGINE_NO()}" />
                </div>
                <div class="col-4 col-border checkbox-mid">
                    <c:choose>
                        <c:when test="${chkVehicle.getChk_auto_engine_no() == '1'}">
                            ถูกต้อง
                        </c:when>
                        <c:when test="${chkVehicle.getChk_auto_engine_no() == '0'}">
                            ไม่ถูกต้อง
                        </c:when>
                    </c:choose>
                </div>
                <div class="col-4 col-border">
                    เลขตัวถัง
                </div>
                <div class="col-4 col-border">
                    <c:out value="${vehicle.getAUTO_BODY_NO()}" />
                </div>
                <div class="col-4 col-border checkbox-mid">
                    <c:choose>
                        <c:when test="${chkVehicle.getChk_auto_body_no() == '1'}">
                            ถูกต้อง
                        </c:when>
                        <c:when test="${chkVehicle.getChk_auto_body_no() == '0'}">
                            ไม่ถูกต้อง
                        </c:when>
                    </c:choose>
                </div>
            </div>
        </div>
        <br><br>
        <div class="container border-shadow" style="text-align: center">
            <div class="row col-left">
                <div class="col-6 col-border">
                    สภาพ
                </div>
                <c:if test="${chkVehicle.getCondition_vehicle() == '1'}">
                    <div class="col-6 col-border">
                        <label class="container checkbox-mid">เดิม
                        </label>
                    </div>
                </c:if>
                <c:if test="${chkVehicle.getCondition_vehicle() == '2'}">
                    <div class="col-6 col-border">
                        <label class="container checkbox-mid">
                            ไม่เดิม
                        </label>
                    </div>
                </c:if>
                <div class="col-6 col-border">
                    ล้อ
                </div>
                <c:if test="${chkVehicle.getWheel() == '1'}">
                    <div class="col-6 col-border">
                        <label class="container checkbox-mid">
                            ล้อแม็ก
                        </label>
                    </div>
                </c:if>
                <c:if test="${chkVehicle.getWheel() == '2'}">
                    <div class="col-6 col-border">
                        <label class="container checkbox-mid">
                            ล้อลวด
                        </label>
                    </div>
                </c:if>
                <div class="col-6 col-border">
                    เบรคหน้า
                </div>
                <c:if test="${chkVehicle.getFrontbreak() == '1'}">
                    <div class="col-6 col-border">
                        <label class="container checkbox-mid">ดิส
                        </label>
                    </div>
                </c:if>
                <c:if test="${chkVehicle.getFrontbreak() == '2'}">
                    <div class="col-6 col-border">
                        <label class="container checkbox-mid">ดรั้ม
                        </label>
                    </div>
                </c:if>
                <div class="col-6 col-border">
                    เบรคหลัง
                </div>
                <c:if test="${chkVehicle.getBackbreak() == '1'}">
                    <div class="col-6 col-border">
                        <label class="container checkbox-mid">ดิส
                        </label>
                    </div>
                </c:if>
                <c:if test="${chkVehicle.getBackbreak() == '2'}">
                    <div class="col-6 col-border">
                        <label class="container checkbox-mid">ดรั้ม
                        </label>
                    </div>
                </c:if>
                <div class="col-6 col-border">
                    ครัช
                </div>
                <c:if test="${chkVehicle.getClutch() == '1'}">
                    <div class="col-6 col-border">
                        <label class="container checkbox-mid">มี
                        </label>
                    </div>
                </c:if>
                <c:if test="${chkVehicle.getClutch() == '2'}">
                    <div class="col-6 col-border">
                        <label class="container checkbox-mid">ไม่มี
                        </label>
                    </div>
                </c:if>
                <div class="col-6 col-border">
                    สตาร์ท
                </div>
                <c:if test="${chkVehicle.getStart() == '1'}">
                    <div class="col-6 col-border">
                        <label class="container checkbox-mid">มือ
                        </label>
                    </div>
                </c:if>
                <c:if test="${chkVehicle.getStart() == '2'}">
                    <div class="col-6 col-border">
                        <label class="container checkbox-mid">เท้า
                        </label>
                    </div>
                </c:if>
                <div class="col-6 col-border">
                    กุญแจ
                </div>
                <c:if test="${chkVehicle.getVehicle_key() == '1'}">
                    <div class="col-6 col-border">
                        <label class="container checkbox-mid">
                            <span class="radiomark"></span>มี
                        </label>
                    </div>
                </c:if>
                <c:if test="${chkVehicle.getVehicle_key() == '2'}">
                    <div class="col-6 col-border">
                        <label class="container checkbox-mid">ไม่มี
                        </label>
                    </div>
                </c:if>



            </div>
        </div>
        <br><br>


        <div class="container border-shadow" style="text-align: center;padding: 5%;">




            <form class="form-check border-shadow" method="post" action="${pageContext.request.contextPath}/savepicture"
                enctype="multipart/form-data">


                <div style="padding:5px; color:red;font-style:italic;">
                    ${errorMessage}
                </div>

                <div class="row">
                    <div class="col-12">
                        <p>ภาพ 1</p>
                    </div>
                    <div class="col-md-4 col-sm-12">
                        <!-- type file must have name -->
                        <label><input id="file" class="btn btn-primary" type="file" name="file" accept=".jpg">
                        </label>
                    </div>
                    <div class="col-md-8 col-sm-12">
                        <c:if test="${pics[0]!=''}"><img id="img" src="${pics[0]}"> <a class="myButton btn btn-danger"
                                style="color: white"
                                onclick="window.location.href='removepicture?ID=${vehicle.getID()}&order=1&keyword=${keyword}'">ลบรูป</a>
                        </c:if>

                    </div>
                </div>
                </br>

                <div class="row">
                    <div class="col-12">
                        <p>ภาพ 2</p>
                    </div>
                    <div class="col-md-4 col-sm-12">
                        <label><input id="file" class="btn btn-primary" type="file" name="file" accept=".jpg">
                        </label>
                    </div>
                    <div class="col-md-8 col-sm-12">
                        <c:if test="${pics[1]!=''}"><img id="img" src="${pics[1]}" /> <a class="myButton btn btn-danger"
                                style="color: white"
                                onclick="window.location.href='removepicture?ID=${vehicle.getID()}&order=2&keyword=${keyword}'">ลบรูป</a>
                        </c:if>
                    </div>
                </div>
                </br>

                <div class="row">
                    <div class="col-12">
                        <p>ภาพ 3</p>
                    </div>
                    <div class="col-md-4 col-sm-12">
                        <label><input id="file" class="btn btn-primary" type="file" name="file" accept=".jpg">
                        </label>
                    </div>
                    <div class="col-md-8 col-sm-12">
                        <c:if test="${pics[2]!=''}"><img id="img" src="${pics[2]}"> <a class="myButton btn btn-danger"
                                style="color: white"
                                onclick="window.location.href='removepicture?ID=${vehicle.getID()}&order=3&keyword=${keyword}'">ลบรูป</a>
                        </c:if>

                    </div>
                </div>
                </br>

                <div class="row">
                    <div class="col-12">
                        <p>ภาพ 4</p>
                    </div>
                    <div class="col-md-4 col-sm-12">
                        <label><input id="file" class="btn btn-primary" type="file" name="file" accept=".jpg">
                        </label>
                    </div>
                    <div class="col-md-8 col-sm-12">
                        <c:if test="${pics[3]!=''}"><img id="img" src="${pics[3]}"> <a class="myButton btn btn-danger"
                                style="color: white"
                                onclick="window.location.href='removepicture?ID=${vehicle.getID()}&order=4&keyword=${keyword}'">ลบรูป</a>
                        </c:if>

                    </div>
                </div>
                </br>

                <div class="row">
                    <div class="col-12">
                        <p>ภาพ 5</p>
                    </div>
                    <div class="col-md-4 col-sm-12">
                        <label><input id="file" class="btn btn-primary" type="file" name="file" accept=".jpg">
                        </label>
                    </div>
                    <div class="col-md-8 col-sm-12">
                        <c:if test="${pics[4]!=''}"><img id="img" src="${pics[4]}"> <a class="myButton btn btn-danger"
                                style="color: white"
                                onclick="window.location.href='removepicture?ID=${vehicle.getID()}&order=5&keyword=${keyword}'">ลบรูป</a>
                        </c:if>

                    </div>
                </div>
                </br>

                <div class="row">
                    <div class="col-12">
                        <p>ภาพ 6</p>
                    </div>
                    <div class="col-md-4 col-sm-12">
                        <label><input id="file" class="btn btn-primary" type="file" name="file" accept=".jpg">
                        </label>
                    </div>
                    <div class="col-md-8 col-sm-12">
                        <c:if test="${pics[5]!=''}"><img id="img" src="${pics[5]}"> <a class="myButton btn btn-danger"
                                style="color: white"
                                onclick="window.location.href='removepicture?ID=${vehicle.getID()}&order=6&keyword=${keyword}'">ลบรูป</a>
                        </c:if>

                    </div>
                </div>
                </br>

                <div class="row">
                    <div class="col-12">
                        <p>ภาพ 7</p>
                    </div>
                    <div class="col-md-4 col-sm-12">
                        <label><input id="file" class="btn btn-primary" type="file" name="file" accept=".jpg">
                        </label>
                    </div>
                    <div class="col-md-8 col-sm-12">
                        <c:if test="${pics[6]!=''}"><img id="img" src="${pics[6]}"> <a class="myButton btn btn-danger"
                                style="color: white"
                                onclick="window.location.href='removepicture?ID=${vehicle.getID()}&order=7&keyword=${keyword}'">ลบรูป</a>
                        </c:if>

                    </div>
                </div>
                </br>

                <div class="row">
                    <div class="col-12">
                        <p>ภาพ 8</p>
                    </div>
                    <div class="col-md-4 col-sm-12">
                        <label><input id="file" class="btn btn-primary" type="file" name="file" accept=".jpg">
                        </label>
                    </div>
                    <div class="col-md-8 col-sm-12">
                        <c:if test="${pics[7]!=''}"><img id="img" src="${pics[7]}"> <a class="myButton btn btn-danger"
                                style="color: white"
                                onclick="window.location.href='removepicture?ID=${vehicle.getID()}&order=8&keyword=${keyword}'">ลบรูป</a>
                        </c:if>

                    </div>
                </div>
                </br>

                <div class="row">
                    <div class="col-12">
                        <p>ภาพ 9</p>
                    </div>
                    <div class="col-md-4 col-sm-12">
                        <label><input id="file" class="btn btn-primary" type="file" name="file" accept=".jpg">
                        </label>
                    </div>
                    <div class="col-md-8 col-sm-12">
                        <c:if test="${pics[8]!=''}"><img id="img" src="${pics[8]}"> <a class="myButton btn btn-danger"
                                style="color: white"
                                onclick="window.location.href='removepicture?ID=${vehicle.getID()}&order=9&keyword=${keyword}'">ลบรูป</a>
                        </c:if>

                    </div>
                </div>
                </br>
                <div class="row">
                    <div class="col-12">
                        <p>ภาพ 10</p>
                    </div>
                    <div class="col-md-4 col-sm-12">
                        <label><input id="file" class="btn btn-primary" type="file" name="file" accept=".jpg">
                        </label>
                    </div>
                    <div class="col-md-8 col-sm-12">
                        <c:if test="${pics[9]!=''}"><img id="img" src="${pics[9]}"> <a class="myButton btn btn-danger"
                                onclick="window.location.href='removepicture?ID=${vehicle.getID()}&order=10&keyword=${keyword}'">ลบรูป</a>
                        </c:if>

                    </div>
                </div>

                <br>

                <input type="hidden" name="ID" value="${vehicle.getID()}">
                <input type="hidden" name="vehicle_id" value="${chkVehicle.getId()}">

                <div style="text-align: center"><input class="btn btn-primary" type="submit" value="Upload"
                        onClick="window.location.reload(true)" /></div>
                <br>

            </form>

    </c:if>
    </div>
    </div>
    <%@ include file="footer.jsp" %>
</body>
${alertSave}
<!--===============================================================================================-->
<script src="LoginNgerntidlor/vendor/jquery/jquery-3.2.1.min.js"></script>
<!--===============================================================================================-->
<script src="LoginNgerntidlor/vendor/bootstrap/js/popper.js"></script>
<script src="/LoginNgerntidlorvendor/bootstrap/js/bootstrap.min.js"></script>
<!--===============================================================================================-->
<script src="/LoginNgerntidlorvendor/select2/select2.min.js"></script>
<!--===============================================================================================-->



</html>
