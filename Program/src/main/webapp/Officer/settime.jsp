﻿<%@ taglib prefix = "c" uri = "http://java.sun.com/jsp/jstl/core" %>
<html>

<head>
    <div w3-include-html="../include/link.jsp"></div>
    <title>Login V12</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="stylesheet" type="text/css" href="LoginNgerntidlor/css/util.css">
    <link rel="stylesheet" type="text/css" href="css/styles.css">
    <!--===============================================================================================-->

    <style>
        .footer {
            position: fixed;
            left: 0;
            bottom: 0;
            width: 100%;

            color: white;
            text-align: center;
        }

    </style>
    <%@ include file="navbarInclude.jsp" %>
</head>
<%@ include file="headOff.jsp" %>

<body style="font-size: 13px">
    <br>
    <div class="container">
        <div class="row">
            <div class="col-12">


                <form class="form-check border-shadow" action="settime" method="post">
                    <span class="p-t-20 p-b-45">
                        <h3 style="padding: 10px; text-align:center">
                            ตั้งเวลาประมูล
                        </h3>
                    </span>


                    <div class="container">
                        <div class="row">
                            <div class="col-md-8">
                                <input class="register-input" type="text" name="keyword"
                                    placeholder="เลขที่สัญญา หรือ เลขทะเบียน" value="${keyword}">

                            </div>

                            <div class="col-md-4" style="margin: auto; text-align: center">
                                <button class="myBtn" onclick="form1.submit();" style="width: 100%">
                                    ค้นหา
                                </button>
                            </div>
                        </div>
                    </div>




                </form>
            </div>
        </div>
    </div>
    <form name="form2" class="container border-shadow " action="savesettime" method="post" style="text-align: center">
        <c:if test="${vehicle.getVehicleBrand()!=null}">
            <br><br>
            <div class="container border-shadow" style="text-align: center">
                <div class="row col-left">
                    <div class="col-4 col-border">
                        ยี่ห้อ
                    </div>
                    <div class="col-4 col-border">
                        รุ่น
                    </div>
                    <div class="col-4 col-border">
                        ทะเบียน
                    </div>
                    <div class="col-4 col-border">
                        <c:out value="${vehicle.getVehicleBrand()}" />
                    </div>
                    <div class="col-4 col-border">
                        <c:out value="${vehicle.getVehicleModel()}" />
                    </div>
                    <div class="col-4 col-border">
                        <c:out value="${vehicle.getRegisteration()}" />
                    </div>
                </div>
            </div>

            <br><br>

            <div class="container border-shadow" style="text-align: center">
                <div class="row col-left">
                    <div class="col-4 col-border">
                        หัวข้อ
                    </div>
                    <div class="col-4 col-border">
                        ข้อมูล
                    </div>
                    <div class="col-4 col-border">
                        ความถูกต้อง
                    </div>
                    <div class="col-4 col-border">
                        เลขที่สัญญา
                    </div>
                    <div class="col-4 col-border">
                        <c:out value="${vehicle.getAgreementNo()}" />
                    </div>
                    <div class="col-4 col-border checkbox-mid">
                        <c:choose>
                            <c:when test="${chkVehicle.getChk_AgreementNo() == '1'}">
                                ถูกต้อง
                            </c:when>
                            <c:when test="${chkVehicle.getChk_AgreementNo() == '0'}">
                                ไม่ถูกต้อง
                            </c:when>
                        </c:choose>
                    </div>
                    <div class="col-4 col-border">
                        เลขเครื่องยนต์
                    </div>
                    <div class="col-4 col-border">
                        <c:out value="${vehicle.getAUTO_ENGINE_NO()}" />
                    </div>
                    <div class="col-4 col-border checkbox-mid">
                        <c:choose>
                            <c:when test="${chkVehicle.getChk_auto_engine_no() == '1'}">
                                ถูกต้อง
                            </c:when>
                            <c:when test="${chkVehicle.getChk_auto_engine_no() == '0'}">
                                ไม่ถูกต้อง
                            </c:when>
                        </c:choose>
                    </div>
                    <div class="col-4 col-border">
                        เลขตัวถัง
                    </div>
                    <div class="col-4 col-border">
                        <c:out value="${vehicle.getAUTO_BODY_NO()}" />
                    </div>
                    <div class="col-4 col-border checkbox-mid">
                        <c:choose>
                            <c:when test="${chkVehicle.getChk_auto_body_no() == '1'}">
                                ถูกต้อง
                            </c:when>
                            <c:when test="${chkVehicle.getChk_auto_body_no() == '0'}">
                                ไม่ถูกต้อง
                            </c:when>
                        </c:choose>
                    </div>
                </div>
            </div>
            <br><br>
            <div class="container border-shadow" style="text-align: center">
                <div class="row col-left">
                    <div class="col-6 col-border">
                        สภาพ
                    </div>
                    <c:if test="${chkVehicle.getCondition_vehicle() == '1'}">
                        <div class="col-6 col-border">
                            <label class="container checkbox-mid">เดิม
                            </label>
                        </div>
                    </c:if>
                    <c:if test="${chkVehicle.getCondition_vehicle() == '2'}">
                        <div class="col-6 col-border">
                            <label class="container checkbox-mid">
                                ไม่เดิม
                            </label>
                        </div>
                    </c:if>
                    <div class="col-6 col-border">
                        ล้อ
                    </div>
                    <c:if test="${chkVehicle.getWheel() == '1'}">
                        <div class="col-6 col-border">
                            <label class="container checkbox-mid">
                                ล้อแม็ก
                            </label>
                        </div>
                    </c:if>
                    <c:if test="${chkVehicle.getWheel() == '2'}">
                        <div class="col-6 col-border">
                            <label class="container checkbox-mid">
                                ล้อลวด
                            </label>
                        </div>
                    </c:if>
                    <div class="col-6 col-border">
                        เบรคหน้า
                    </div>
                    <c:if test="${chkVehicle.getFrontbreak() == '1'}">
                        <div class="col-6 col-border">
                            <label class="container checkbox-mid">ดิส
                            </label>
                        </div>
                    </c:if>
                    <c:if test="${chkVehicle.getFrontbreak() == '2'}">
                        <div class="col-6 col-border">
                            <label class="container checkbox-mid">ดรั้ม
                            </label>
                        </div>
                    </c:if>
                    <div class="col-6 col-border">
                        เบรคหลัง
                    </div>
                    <c:if test="${chkVehicle.getBackbreak() == '1'}">
                        <div class="col-6 col-border">
                            <label class="container checkbox-mid">ดิส
                            </label>
                        </div>
                    </c:if>
                    <c:if test="${chkVehicle.getBackbreak() == '2'}">
                        <div class="col-6 col-border">
                            <label class="container checkbox-mid">ดรั้ม
                            </label>
                        </div>
                    </c:if>
                    <div class="col-6 col-border">
                        ครัช
                    </div>
                    <c:if test="${chkVehicle.getClutch() == '1'}">
                        <div class="col-6 col-border">
                            <label class="container checkbox-mid">มี
                            </label>
                        </div>
                    </c:if>
                    <c:if test="${chkVehicle.getClutch() == '2'}">
                        <div class="col-6 col-border">
                            <label class="container checkbox-mid">ไม่มี
                            </label>
                        </div>
                    </c:if>
                    <div class="col-6 col-border">
                        สตาร์ท
                    </div>
                    <c:if test="${chkVehicle.getStart() == '1'}">
                        <div class="col-6 col-border">
                            <label class="container checkbox-mid">มือ
                            </label>
                        </div>
                    </c:if>
                    <c:if test="${chkVehicle.getStart() == '2'}">
                        <div class="col-6 col-border">
                            <label class="container checkbox-mid">เท้า
                            </label>
                        </div>
                    </c:if>
                    <div class="col-6 col-border">
                        กุญแจ
                    </div>
                    <c:if test="${chkVehicle.getVehicle_key() == '1'}">
                        <div class="col-6 col-border">
                            <label class="container checkbox-mid">มี
                            </label>
                        </div>
                    </c:if>
                    <c:if test="${chkVehicle.getVehicle_key() == '2'}">
                        <div class="col-6 col-border">
                            <label class="container checkbox-mid">ไม่มี
                            </label>
                        </div>
                    </c:if>
                </div>
            </div>
            <br><br>
            <div class="container border-shadow" style="text-align: center;padding: 5%;">
                <div class="row">
                    <div class="col-6" style="text-align: right;">
                        หมายเหตุ
                    </div>
                    <div class="col-6" style="text-align: left;">
                        <label>
                            <c:out value="${chkVehicle.getConditionReport()}" />
                        </label>
                    </div>
                </div>
                <div class="row">
                    <div class="col-6" style="text-align: right;">
                        วันที่เริ่มประมูล
                    </div>
                    <div class="col-6" style="text-align: left;">
                        <label><input type="date" name="use_date" value="${setTime.getDate()}" required>
                        </label>
                    </div>
                </div>
                <div class="row">
                    <div class="col-6" style="text-align: right;">
                        เวลาเริ่มประมูล
                    </div>
                    <div class="col-6" style="text-align: left;">
                        <label><input type="time" name="use_time" value="${setTime.getTime_begin()}" required>
                        </label>
                    </div>
                </div>
                <div class="row">
                    <div class="col-6" style="text-align: right;">
                        วันที่จบประมูล
                    </div>
                    <div class="col-6" style="text-align: left;">
                        <label><input type="date" name="end_date" value="${setTime.getDate_end()}" required>
                        </label>
                    </div>
                </div>
                <div class="row">
                    <div class="col-6" style="text-align: right;">
                        เวลาจบประมูล
                    </div>
                    <div class="col-6" style="text-align: left;">
                        <label><input type="time" name="end_time" value="${setTime.getTime_end()}" required>
                        </label>
                    </div>
                </div>
                <div class="row">
                    <div class="col-6" style="text-align: right;">
                        ลำดับเข้าขาย
                    </div>
                    <div class="col-6" style="text-align: left;">
                        <label><input class="input100" type="number" name="ordersell" placeholder="ลำดับเข้าขาย"
                                value="${setTime.getOrdersell()}">
                        </label>
                    </div>
                </div>
                <div class="row">
                    <div class="col-6" style="text-align: right;">
                        ราคาประเมิน
                    </div>
                    <div class="col-6" style="text-align: left;">
                        <label><input class="input100" type="number" name="pricestart" placeholder="ราคาประเมิน"
                                value="${setTime.getPricestart()}">
                        </label>
                    </div>
                </div>
                <div style="text-align: center">
                    <!-- <button class="myBtn" style="width: 100px" onclick="form2.submit();">
                        บันทึก
                    </button> -->
                    <input type="submit" class="myBtn" style="width: 100px" value="บันทึก">
                    <input type="hidden" name="vehicle_id" value="${vehicle.getID()}">
                    <input type="hidden" name="settimeid" value="${setTime.getId()}">
    </form>

    </div>
    <br><br><br>



    </c:if>
    </div>
    </div>

    <%@ include file="footer.jsp" %>
</body>
${alertSave}
<!--===============================================================================================-->
<script src="LoginNgerntidlor/vendor/jquery/jquery-3.2.1.min.js"></script>
<!--===============================================================================================-->
<script src="LoginNgerntidlor/vendor/bootstrap/js/popper.js"></script>
<script src="LoginNgerntidlorvendor/bootstrap/js/bootstrap.min.js"></script>
<!--===============================================================================================-->
<script src="LoginNgerntidlorvendor/select2/select2.min.js"></script>
<!--===============================================================================================-->
<!-- <script src="LoginNgerntidlor/js/main.js"></script> -->

</html>
