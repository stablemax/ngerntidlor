﻿    <%@ taglib prefix = "c" uri = "http://java.sun.com/jsp/jstl/core" %>



<nav class="navbar navbar-expand-md navbar-dark navbar-fixed-top" style="background-color: #0060ab; font-size:16px;">
    <a class="navbar-brand" href="index"><img
            src="https://www.ngerntidlor.com/NTL/media/NTL-Library/Logo/logo-ntl-2018-rgb@2x.png" width="120px"></a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#collapsibleNavbar">
        <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="collapsibleNavbar">
        <ul class="navbar-nav ml-auto">
            <li class="nav-item">
                <a class="nav-link text-white" href="importcsv">นำเข้าข้อมูลรถ</a>
            </li>
            <li class="nav-item">
                <a class="nav-link text-white" href="checkvehicle">ตรวจสภาพรถ</a>
            </li>
            <li class="nav-item">
                <a class="nav-link text-white" href="exportcsv2">ดาวน์โหลดข้อมูลตรวจสภาพรถ</a>
            </li>
            <li class="nav-item">
                <a class="nav-link text-white" href="upload">อัพโหลดรูปรถเข้าประมูลออนไลน์</a>
            </li>
            <li class="nav-item">
                <a class="nav-link text-white" href="settime">ตั้งเวลาประมูล</a>
            </li>
            <li class="nav-item">
                <a class="nav-link text-white" href="calendar">ปฏิทินการประมูล</a>
            </li>
            <li class="nav-item">
                <a class="nav-link text-white" href="listoff">แคตตาล็อครถ</a>
            </li>
            <li class="nav-item">
                <a class="nav-link text-white" href="receipt">ใบเสร็จ</a>
            </li>
            <li class="nav-item">
                <a class="nav-link text-white" href="logout">ลงชื่อออก</a>
            </li>
        </ul>
    </div>
</nav>
