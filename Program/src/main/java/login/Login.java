package login;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.mysql.cj.jdbc.Driver;

import org.apache.commons.codec.digest.DigestUtils;

import entity.User;

public class Login extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        // can input Thai character
        req.setCharacterEncoding("UTF-8");
        resp.setContentType("text/html;charset=UTF-8");

        HttpSession session = req.getSession();
        if (session.getAttribute("id") != null) {
            req.getRequestDispatcher("/index").forward(req, resp);
        } else {
            req.getRequestDispatcher("/LoginNgerntidlor/login.jsp").forward(req, resp);
        }
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        HttpSession session = req.getSession();
        if (session.getAttribute("id") != null) {
            req.setAttribute("output",
                    "Welcome " + session.getAttribute("firstname") + " " + session.getAttribute("lastname"));
            req.getRequestDispatcher("/LoginNgerntidlor/index.jsp").forward(req, resp);
        } else if (req.getParameter("username") == null && req.getParameter("password") == null) {
            req.getRequestDispatcher("/LoginNgerntidlor/login.jsp").forward(req, resp);
        }
        String username = req.getParameter("username");

        String password = DigestUtils.sha256Hex(req.getParameter("password"));

        User user = new User();
        boolean check2 = false;
        check2 = user.counter(username, password);

        if (!check2) {

            req.setAttribute("output", "บัญชีของคุณถูกระงับชั่วคราว กรุณาติดต่อเจ้าหน้าที่");
            req.getRequestDispatcher("/LoginNgerntidlor/login.jsp").forward(req, resp);

        } else {

            boolean check = false;
            check = user.getLogin(username, password, req);
            if (!check) {

                req.setAttribute("output", "ชื่อผู้ใช้ หรือ รหัสผ่านไม่ถูกต้อง");
                req.getRequestDispatcher("/LoginNgerntidlor/login.jsp").forward(req, resp);

            } else {
                // req.getRequestDispatcher("/main").forward(req, resp);
                req.getRequestDispatcher("/index").forward(req, resp);

            }
        }

    }
}