package login;

import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.Statement;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.codec.digest.DigestUtils;

import entity.User;

public class Register extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        // can input Thai character
        req.setCharacterEncoding("UTF-8");
        resp.setContentType("text/html;charset=UTF-8");

        HttpSession session = req.getSession();
        if (session.getAttribute("id") == null) {
            req.getRequestDispatcher("/LoginNgerntidlor/register.jsp").forward(req, resp);
        } else {
            req.setAttribute("output",
                    "Welcome " + session.getAttribute("firstname") + " " + session.getAttribute("lastname"));
            req.getRequestDispatcher("/LoginNgerntidlor/index_customer.jsp").forward(req, resp);
        }
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        // can input Thai character
        req.setCharacterEncoding("UTF-8");
        resp.setContentType("text/html;charset=UTF-8");

        HttpSession session = req.getSession();
        if (session.getAttribute("id") != null) {
            req.setAttribute("output",
                    "Welcome " + session.getAttribute("firstname") + " " + session.getAttribute("lastname"));
            req.getRequestDispatcher("/LoginNgerntidlor/index_customer.jsp").forward(req, resp);
        }
        String username = req.getParameter("username");
        User user = new User();
        boolean check2 = false;
        check2 = user.userExist(username);

        if (!check2) {
            // System.out.println("ifffffffff");
            req.setAttribute("output", "ชื่อผู้ใช้นี้มีอยู่แล้ว กรุณาใช้ชื่อผู้ใช้อื่น");

            req.setAttribute("firstname", req.getParameter("firstname"));
            req.setAttribute("lastname", req.getParameter("lastname"));
            req.setAttribute("idcard", req.getParameter("idcard"));
            req.setAttribute("tel", req.getParameter("tel"));
            req.setAttribute("email", req.getParameter("email"));
            req.setAttribute("address", req.getParameter("address"));
            req.setAttribute("line", req.getParameter("line"));
            req.setAttribute("storename", req.getParameter("storename"));
            req.setAttribute("username", req.getParameter("username"));
            req.setAttribute("password", req.getParameter("password"));

            req.getRequestDispatcher("/LoginNgerntidlor/register.jsp").forward(req, resp);

        } else { // boolean check3 = false;
            // user.userCheck(req.getParameter("username"));
            // if (!check3) {
            // System.out.println("elseeeeeeeeeee");
            // req.setAttribute("userCheck", "This username already been taken");

            // } else {

            user.register(req.getParameter("username"), req.getParameter("password"), req.getParameter("firstname"),
                    req.getParameter("lastname"), req.getParameter("idcard"), req.getParameter("tel"),
                    req.getParameter("address"), req.getParameter("email"), req.getParameter("line"),
                    req.getParameter("storename"));

            String password = DigestUtils.sha256Hex(req.getParameter("password"));

            boolean check = false;
            check = user.getLogin(username, password, req);

            req.setAttribute("output",
                    "Welcome " + session.getAttribute("firstname") + " " + session.getAttribute("lastname"));
            req.getRequestDispatcher("/LoginNgerntidlor/index_customer.jsp").forward(req, resp);
        }

        // }

    }

}
