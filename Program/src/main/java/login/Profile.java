package login;

import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.Statement;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import entity.User;

public class Profile extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        HttpSession session = req.getSession();
        if (session.getAttribute("id") == null) {
            req.getRequestDispatcher("/LoginNgerntidlor/login.jsp").forward(req, resp);
        } else {

            // req.setAttribute("output",
            // "Welcome " + session.getAttribute("firstname") + " " +
            // session.getAttribute("lastname"));

            // req.setAttribute("firstname", session.getAttribute("firstname"));
            // req.setAttribute("lastname", session.getAttribute("lastname"));
            // req.setAttribute("idcard", session.getAttribute("idcard"));
            // req.setAttribute("tel", session.getAttribute("tel"));
            // req.setAttribute("email", session.getAttribute("email"));
            // req.setAttribute("address", session.getAttribute("address"));
            // req.setAttribute("line", session.getAttribute("line"));
            // req.setAttribute("storename", session.getAttribute("storename"));
            // req.setAttribute("username", session.getAttribute("username"));
            // req.setAttribute("password", session.getAttribute("password"));
            User user = new User();
            user = user.getProfile(session.getAttribute("username").toString());

            // System.out.println(user.getFirstname());
            // System.out.println(user.getIdcard());

            req.setAttribute("user", user);
            req.getRequestDispatcher("/LoginNgerntidlor/profile.jsp").forward(req, resp);
        }
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        // can input Thai character
        req.setCharacterEncoding("UTF-8");
        resp.setContentType("text/html;charset=UTF-8");

        HttpSession session = req.getSession();
        if (session.getAttribute("id") == null) {
            req.getRequestDispatcher("/login").forward(req, resp);
        } else {

            User user = new User();

            user.profile(req.getParameter("username"), req.getParameter("password"), req.getParameter("firstname"),
                    req.getParameter("lastname"), req.getParameter("idcard"), req.getParameter("tel"),
                    req.getParameter("address"), req.getParameter("email"), req.getParameter("line"),
                    req.getParameter("storename"), req);

            if (req.getParameter("password") != "") {

                user.resetPassword(req.getParameter("username"), req.getParameter("password"), req);

            }
            // String username = req.getParameter("username");
            // String password = req.getParameter("password");
            user.getProfile(req.getParameter("username").toString());

            req.setAttribute("user", user);

            req.getRequestDispatcher("/LoginNgerntidlor/profile.jsp").forward(req, resp);
        }
    }

}
