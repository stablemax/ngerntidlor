package officer;

import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.Statement;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import entity.Auction;
import entity.User;
import entity.Vehicle;

public class ListOff extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        // can input Thai character
        req.setCharacterEncoding("UTF-8");
        resp.setContentType("text/html;charset=UTF-8");

        HttpSession session = req.getSession();
        if (session.getAttribute("id") == null) {
            req.getRequestDispatcher("/login").forward(req, resp);
        } else {

            Vehicle vehicle = new Vehicle();
            ArrayList<Vehicle> vehicles;
            if (req.getParameter("brand") != null || req.getParameter("veheclemodel") != null
                    || req.getParameter("year") != null) {

                vehicles = vehicle.searchVehicleCatalog(req.getParameter("brand"), req.getParameter("veheclemodel"),
                        req.getParameter("year"));
            } else {
                vehicles = vehicle.getAllVehicle();

            }

            ArrayList<String> brands = vehicle.getAllBrand();
            ArrayList<String> vehecle_models = vehicle.getAllModel();
            ArrayList<String> years = vehicle.getAllYear();

            req.setAttribute("brands", brands);
            req.setAttribute("vehecle_models", vehecle_models);
            req.setAttribute("years", years);

            req.setAttribute("s_registeration", req.getParameter("registeration"));
            req.setAttribute("s_brand", req.getParameter("brand"));
            req.setAttribute("s_vehecle_model", req.getParameter("veheclemodel"));
            req.setAttribute("s_year", req.getParameter("year"));

            req.setAttribute("vehicles", vehicles);

            req.getRequestDispatcher("/Officer/listoff.jsp").forward(req, resp);
        }
    }
}