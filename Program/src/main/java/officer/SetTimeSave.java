package officer;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Scanner;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.taglibs.standard.tag.common.fmt.SetTimeZoneSupport;

import entity.ChkVehicle;
import entity.SetTime;
import entity.Vehicle;

public class SetTimeSave extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        // can input Thai character
        req.setCharacterEncoding("UTF-8");
        resp.setContentType("text/html;charset=UTF-8");

        HttpSession session = req.getSession();
        if (session.getAttribute("id") == null) {
            req.getRequestDispatcher("/login").forward(req, resp);
        } else {
            req.getRequestDispatcher("/Officer/settime.jsp").forward(req, resp);
        }
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        // can input Thai character
        req.setCharacterEncoding("UTF-8");
        resp.setContentType("text/html;charset=UTF-8");

        HttpSession session = req.getSession();
        if (session.getAttribute("id") == null) {
            req.getRequestDispatcher("/login").forward(req, resp);
        } else {
            SetTime setTime = new SetTime();
            String alertSave;
            String set_id = req.getParameter("settimeid");
            if (!set_id.equals("") && !set_id.equals("0")) {
                setTime.updateSetTime(Integer.parseInt(req.getParameter("settimeid").toString()),
                        Integer.parseInt(req.getParameter("vehicle_id").toString()),
                        req.getParameter("use_time").toString(), req.getParameter("end_time").toString(),
                        req.getParameter("use_date").toString(), req.getParameter("end_date").toString(),
                        Integer.parseInt(req.getParameter("ordersell").toString()),
                        BigDecimal.valueOf(Integer.parseInt(req.getParameter("pricestart").toString())));
                alertSave = "<script>alert('แก้ไขสำเร็จ');</script>";
            } else {
                setTime.addSetTime(Integer.parseInt(req.getParameter("vehicle_id").toString()),
                        req.getParameter("use_time").toString(), req.getParameter("end_time").toString(),
                        req.getParameter("use_date").toString(), req.getParameter("end_date").toString(),
                        Integer.parseInt(req.getParameter("ordersell").toString()),
                        BigDecimal.valueOf(Integer.parseInt(req.getParameter("pricestart").toString())));

                alertSave = "<script>alert('บันทึกสำเร็จ');</script>";
            }
            req.setAttribute("alertSave", alertSave);
            req.getRequestDispatcher("/Officer/settime.jsp").forward(req, resp);
        }
    }
}
