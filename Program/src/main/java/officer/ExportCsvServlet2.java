
package officer;

import java.io.IOException;
import java.io.OutputStream;
import java.sql.Connection;

import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Date;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.mysql.cj.jdbc.Driver;

import org.apache.catalina.User;

import config.DBConnection;

import com.fasterxml.jackson.databind.ObjectMapper;

import entity.ChkVehicle;
import entity.Vehicle;

public class ExportCsvServlet2 extends HttpServlet {

    @Override
    public void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String UTF8_BOM = "\uFEFF";
        // can input Thai character
        req.setCharacterEncoding("UTF-8");
        resp.setContentType("text/html;charset=UTF-8");

        HttpSession session = req.getSession();
        if (session.getAttribute("id") == null) {
            req.getRequestDispatcher("/login").forward(req, resp);
        } else {
            resp.setContentType("text/csv");
            resp.setHeader("Content-Disposition", "attachment; filename=\"userDirectory.csv\"");
            DBConnection dbConnection = DBConnection.getInstance();

            String[] param = new String[] {};
            String sql = "select * from vehicle inner join chkvehicle on chkvehicle.vehicleid = vehicle.id;";

            ResultSet resultSet = dbConnection.dataQueryGet(sql, param);

            try {
                OutputStream outputStream = resp.getOutputStream();
                String outputResult = UTF8_BOM
                        + " เลขที่สัญญา, ยี่ห้อ, รุ่น, สี, ปี, เลขทะเบียน, เลขทะเบียนจังหวัด, เลขเครื่องยนต์, เลขตัวถัง,ความถูกต้องเลขสัญญา,ความถูกต้องเลขเครื่องยนต์,ความถูกต้องเลขตัวถัง,สภาพ,ล้อ,เบรคหน้า,เบรคหลัง,ครัช,สตร์ท,กุญแจ,ลำดับเข้าขาย,ราคาประเมิน,หมายเหตุ\n";
                while (resultSet.next()) {

                    outputResult += resultSet.getString("AgreementNo") + ",";
                    outputResult += resultSet.getString("VehicleBrand") + "," + resultSet.getString("VehicleModel")
                            + ",";
                    outputResult += resultSet.getString("VehicleColor") + "," + resultSet.getInt("VehicleYear") + ","
                            + resultSet.getString("Registeration") + "," + resultSet.getString("RegisterationProvince")
                            + "," + resultSet.getString("AUTO_ENGINE_NO") + "," + resultSet.getString("AUTO_BODY_NO");

                    if (resultSet.getInt("chk_agreementno") == 1) {
                        outputResult += "," + "ตรง";
                    } else if (resultSet.getInt("chk_agreementno") == 0) {
                        outputResult += "," + "ไม่ตรง";

                    } else {
                        outputResult += ",";

                    }

                    if (resultSet.getInt("chk_auto_engine_no") == 1) {
                        outputResult += "," + "ตรง";
                    } else if (resultSet.getInt("chk_auto_engine_no") == 0) {
                        outputResult += "," + "ไม่ตรง";

                    } else {
                        outputResult += ",";

                    }

                    if (resultSet.getInt("chk_auto_body_no") == 1) {
                        outputResult += "," + "ตรง";
                    } else if (resultSet.getInt("chk_auto_body_no") == 0) {
                        outputResult += "," + "ไม่ตรง";

                    } else {
                        outputResult += ",";

                    }

                    if (resultSet.getInt("condition_vehicle") == 1) {
                        outputResult += "," + "เดิม";
                    } else if (resultSet.getInt("condition_vehicle") == 2) {
                        outputResult += "," + "ไม่เดิม";

                    } else {
                        outputResult += ",";

                    }

                    if (resultSet.getInt("wheel") == 1) {
                        outputResult += "," + "ล้อแม็ก";
                    } else if (resultSet.getInt("wheel") == 2) {
                        outputResult += "," + "ล้อลวด";

                    } else {
                        outputResult += ",";

                    }

                    if (resultSet.getInt("frontbreak") == 1) {
                        outputResult += "," + "ดิส";
                    } else if (resultSet.getInt("frontbreak") == 2) {
                        outputResult += "," + "ดรั้ม";

                    } else {
                        outputResult += ",";

                    }

                    if (resultSet.getInt("backbreak") == 1) {
                        outputResult += "," + "ดิส";
                    } else if (resultSet.getInt("backbreak") == 2) {
                        outputResult += "," + "ดรั้ม";

                    } else {
                        outputResult += ",";

                    }

                    if (resultSet.getInt("clutch") == 1) {
                        outputResult += "," + "มี";
                    } else if (resultSet.getInt("clutch") == 2) {
                        outputResult += "," + "ไม่มี";

                    } else {
                        outputResult += ",";

                    }

                    if (resultSet.getInt("start") == 1) {
                        outputResult += "," + "มือ";
                    } else if (resultSet.getInt("start") == 2) {
                        outputResult += "," + "เท้า";

                    } else {
                        outputResult += ",";

                    }

                    if (resultSet.getInt("vehicle_key") == 1) {
                        outputResult += "," + "มี";
                    } else if (resultSet.getInt("start") == 2) {
                        outputResult += "," + "ไม่มี";

                    } else {
                        outputResult += ",";

                    }
                    outputResult += "," + resultSet.getInt("ordersell") + "," + resultSet.getInt("pricestart");
                    outputResult += "," + resultSet.getString("ConditionReport") + "\n";
                }
                outputStream.write(outputResult.getBytes());
                outputStream.flush();
                outputStream.close();
            } catch (Exception e) {
                System.out.println(e.toString());
            }
        }
    }
}
