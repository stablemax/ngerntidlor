package officer;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Scanner;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import entity.ChkVehicle;
import entity.Vehicle;

//testcommit
public class CheckVehicleServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        // can input Thai character
        req.setCharacterEncoding("UTF-8");
        resp.setContentType("text/html;charset=UTF-8");

        HttpSession session = req.getSession();
        if (session.getAttribute("id") == null) {
            req.getRequestDispatcher("/login").forward(req, resp);
        } else {
            req.getRequestDispatcher("/Officer/checkvehicle.jsp").forward(req, resp);
        }
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        // can input Thai character
        req.setCharacterEncoding("UTF-8");
        resp.setContentType("text/html;charset=UTF-8");

        HttpSession session = req.getSession();
        if (session.getAttribute("id") == null) {
            req.getRequestDispatcher("/login").forward(req, resp);
        } else {

            String keyword = req.getParameter("keyword");
            Vehicle vehicle = new Vehicle();
            ChkVehicle chkVehicle = new ChkVehicle();
            vehicle.searchVehicle(keyword);
            chkVehicle.getChkVehicleByID(Integer.toString(vehicle.getID()));
            if (keyword.equals(""))
                vehicle = null;
            req.setAttribute("vehicle", vehicle);
            req.setAttribute("chkVehicle", chkVehicle);
            req.setAttribute("keyword", keyword);

            req.getRequestDispatcher("/Officer/checkvehicle.jsp").forward(req, resp);
        }
    }
}
