package customer;

import java.io.File;
import java.io.IOException;
import java.io.OutputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.Statement;
import java.util.ArrayList;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import entity.Auction;
import entity.AuctionCounter;
import entity.SetTime;
import entity.User;
import entity.Vehicle;

public class AuctionServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        // can input Thai character
        req.setCharacterEncoding("UTF-8");
        resp.setContentType("text/html;charset=UTF-8");

        HttpSession session = req.getSession();
        if (session.getAttribute("id") == null) {
            req.getRequestDispatcher("/login").forward(req, resp);
        } else {
            Vehicle vehicle = new Vehicle();
            ServletContext context = req.getServletContext();
            String path = context.getRealPath("/");

            String id = req.getParameter("id");

            String fileName = "";
            String pics = new String();
            vehicle.getVehicleById(Integer.parseInt(id));

            fileName = String.valueOf(path + "uploadDir/" + vehicle.getID() + "." + 1 + ".jpg");

            File checkfile = new File(fileName);
            if (checkfile.exists()) {
                pics += "uploadDir/" + vehicle.getID() + "." + 1 + ".jpg";
            }

            SetTime setTime = new SetTime();
            setTime.getSetTimeByVegicle(id);

            AuctionCounter auctionCounter = new AuctionCounter();
            int counter = auctionCounter.viewCounter(Integer.parseInt(id));

            // ArrayList<Vehicle> vehicles = vehicle.getAllVehicle();
            Auction auction = new Auction();
            auction.getAuctionUserVehicle(Integer.parseInt(id),
                    Integer.parseInt(session.getAttribute("id").toString()));

            if (auction.getId() == 0) {
                auction.setPriceauction(setTime.getPricestart());
            }
            int countauction = 0;
            if (auction.countAuction(Integer.parseInt(id)) > 0)
                countauction = auction.countAuction(Integer.parseInt(id));

            vehicle.getVehicleById(Integer.parseInt(id));
            setTime.getPricestart();
            req.setAttribute("auction", auction);
            req.setAttribute("counter", counter);
            req.setAttribute("countauction", countauction);
            req.setAttribute("setTime", setTime);
            req.setAttribute("vehicle", vehicle);
            req.setAttribute("pics", pics);
            req.getRequestDispatcher("/LoginNgerntidlor/AuctionForm.jsp").forward(req, resp);
        }
    }
}