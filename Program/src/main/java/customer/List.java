package customer;

import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.Statement;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import entity.User;
import entity.Vehicle;

public class List extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        // can input Thai character
        req.setCharacterEncoding("UTF-8");
        resp.setContentType("text/html;charset=UTF-8");

        HttpSession session = req.getSession();
        if (session.getAttribute("id") == null) {
            req.getRequestDispatcher("/login").forward(req, resp);
        } else {
            int page;
            if (req.getParameter("page") != null) {
                page = Integer.parseInt(req.getParameter("page"));
            } else {
                page = 1;
            }

            // System.out.println("page= " + page);

            Vehicle vehicle = new Vehicle();
            ArrayList<Vehicle> vehicles;

            int numpage = 0;

            if (req.getParameter("brand") != null || req.getParameter("veheclemodel") != null
                    || req.getParameter("year") != null) {

                // System.out.println("num vehicle: " +
                // vehicle.getCountVehicleWhere(req.getParameter("brand"),
                // req.getParameter("veheclemodel"), req.getParameter("year")));

                numpage = vehicle.getCountVehicleWhere(req.getParameter("brand"), req.getParameter("veheclemodel"),
                        req.getParameter("year")) / 12;
                vehicles = vehicle.searchVehicleCatalog(req.getParameter("brand"), req.getParameter("veheclemodel"),
                        req.getParameter("year"));
            } else {
                // System.out.println("num vehicle: " + vehicle.getCountVehicle());

                numpage = vehicle.getCountVehicle() / 12;
                vehicles = vehicle.getAllVehicle();
            }

            // int numpage = vehicles.size() / 12;

            // System.out.println("numpage= " + numpage);

            int pagestart;
            int pageend = page + 2;

            // if (page <= 2) {
            // pagestart = 1;
            // pageend += 3 - page;
            // } else {
            // pagestart = page - 2;
            // if (numpage < pageend) {
            // pageend = numpage;
            // pagestart = numpage - 5;
            // }
            // }

            if (numpage == 0) {
                pagestart = 1;
                pageend = 1;
            } else if (page <= 2) {
                pagestart = 1;
                pageend += 3 - page;
            } else {
                pagestart = page - 2;
                if (numpage < pageend) {
                    pageend = numpage;
                    pagestart = numpage - 5;
                }
            }

            // current page
            // System.out.println("page= " + page);

            // System.out.println("pagestart= " + pagestart);
            // System.out.println("pageend= " + pageend);
            // System.out.println("--------------------------------------");

            ArrayList<String> brands = vehicle.getAllBrand();
            ArrayList<String> vehecle_models = vehicle.getAllModel();
            ArrayList<String> years = vehicle.getAllYear();

            req.setAttribute("brands", brands);
            req.setAttribute("vehecle_models", vehecle_models);
            req.setAttribute("years", years);

            req.setAttribute("s_registeration", req.getParameter("registeration"));
            req.setAttribute("s_brand", req.getParameter("brand"));
            req.setAttribute("s_vehecle_model", req.getParameter("veheclemodel"));
            req.setAttribute("s_year", req.getParameter("year"));

            // req.setAttribute("pagestart", pagestart);
            // req.setAttribute("pageend", pageend);
            // req.setAttribute("page", page);
            // req.setAttribute("numpage", numpage);
            req.setAttribute("vehicles", vehicles);

            req.getRequestDispatcher("/LoginNgerntidlor/list.jsp").forward(req, resp);
        }
    }
}