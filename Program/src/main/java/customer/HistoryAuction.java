package customer;

import java.io.File;
import java.io.IOException;
import java.io.OutputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.Statement;
import java.util.ArrayList;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import entity.History;
import entity.SetTime;
import entity.User;
import entity.Vehicle;

public class HistoryAuction extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        // can input Thai character
        req.setCharacterEncoding("UTF-8");
        resp.setContentType("text/html;charset=UTF-8");

        HttpSession session = req.getSession();
        if (session.getAttribute("id") == null) {
            req.getRequestDispatcher("/login").forward(req, resp);
        } else {
            History history = new History();
            ArrayList<History> histories = history
                    .getHistories(Integer.parseInt(session.getAttribute("id").toString()));
            User user = new User();

            System.out.println(histories.get(0).getId());

            req.setAttribute("session", session);
            req.setAttribute("histories", histories);

            req.getRequestDispatcher("/LoginNgerntidlor/History.jsp").forward(req, resp);
        }
    }
}