package entity;

import java.sql.ResultSet;
import java.sql.SQLException;

import com.fasterxml.jackson.annotation.JsonTypeInfo.Id;

import config.DBConnection;

public class AuctionCounter {
    private int id;
    private int vehicleid;
    private int counter;
    private DBConnection dbConnection;

    public AuctionCounter() {
    }

    public AuctionCounter(int id, int vehicleid, int counter) {
        this.id = id;
        this.vehicleid = vehicleid;
        this.counter = counter;
    }

    public int getId() {
        return this.id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getVehicleid() {
        return this.vehicleid;
    }

    public void setVehicleid(int vehicleid) {
        this.vehicleid = vehicleid;
    }

    public int getCounter() {
        return this.counter;
    }

    public void setCounter(int counter) {
        this.counter = counter;
    }

    public int viewCounter(int vehicleid) {

        this.dbConnection = DBConnection.getInstance();
        String[] param = new String[] { Integer.toString(vehicleid) };
        String sql = "SELECT * FROM auctioncounter WHERE vehicleid = ?  ";
        ResultSet resultSet = this.dbConnection.dataQueryGet(sql, param);
        try {
            while (resultSet.next()) {
                this.setId(resultSet.getInt("id"));
                this.setCounter(resultSet.getInt("counter"));
                this.setVehicleid(resultSet.getInt("vehicleid"));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        if (this.counter == 0) {
            param = new String[] { Integer.toString(vehicleid), "1" };
            sql = "INSERT INTO auctioncounter (vehicleid,counter) VALUE (?,?)";
            this.dbConnection.updateQuery(sql, param);

        } else {
            this.counter += 1;
            param = new String[] { Integer.toString(this.counter), Integer.toString(this.id) };
            sql = "UPDATE auctioncounter set counter = ? where id = ? ";
            this.dbConnection.updateQuery(sql, param);
        }
        return this.counter;
    }
}