package entity;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Objects;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import config.DBConnection;

public class User {
    private int userid;
    private String username;
    private String password;
    private String firstname;
    private String lastname;
    private String roleid;
    private String idcard;
    private String tel;
    private String address;
    private String email;
    private String line;
    private String storename;
    private DBConnection dbConnection;
    private int counter;

    public User() {
    }

    public User(int userid, String username, String password, String firstname, String lastname, String roleid,
            String idcard, String tel, String address, String email, String line, String storename,
            DBConnection dbConnection, int counter) {
        this.userid = userid;
        this.username = username;
        this.password = password;
        this.firstname = firstname;
        this.lastname = lastname;
        this.roleid = roleid;
        this.idcard = idcard;
        this.tel = tel;
        this.address = address;
        this.email = email;
        this.line = line;
        this.storename = storename;
        this.dbConnection = dbConnection;
        this.counter = counter;
    }

    public User(String username, String password, String firstname, String lastname, String idcard, String tel,
            String address, String email, String line, String storename) {
        this.username = username;
        this.password = password;
        this.firstname = firstname;
        this.lastname = lastname;
        this.idcard = idcard;
        this.tel = tel;
        this.address = address;
        this.email = email;
        this.line = line;
        this.storename = storename;
    }

    public User(int userid, String username, String password, String firstname, String lastname, String roleid,
            String idcard, String tel, String address, String email, String line, String storename,
            DBConnection dbConnection) {
        this.userid = userid;
        this.username = username;
        this.password = password;
        this.firstname = firstname;
        this.lastname = lastname;
        this.roleid = roleid;
        this.idcard = idcard;
        this.tel = tel;
        this.address = address;
        this.email = email;
        this.line = line;
        this.storename = storename;
        this.dbConnection = dbConnection;
    }

    // public boolean userCheck(String username) {

    // this.dbConnection = DBConnection.getInstance();
    // String[] param = new String[] { username };
    // String sql = "SELECT * FROM users WHERE username = ?";
    // ResultSet resultSet = this.dbConnection.dataQueryGet(sql, param);
    // boolean check3 = false;

    // System.out.println(username);

    // try {
    // while (resultSet.next()) {

    // if (username.equals(resultSet.getString("username"))) {

    // System.out.println(resultSet.getString("username"));

    // System.out.println("This username already used");

    // }
    // }
    // } catch (SQLException e) {
    // // TODO Auto-generated catch block
    // e.printStackTrace();
    // }
    // return check3;

    // }

    public boolean counter(String username, String password) {

        this.dbConnection = DBConnection.getInstance();
        String[] param = new String[] { username };
        String sql = "SELECT * FROM users WHERE username = ?";
        ResultSet resultSet = this.dbConnection.dataQueryGet(sql, param);
        boolean check2 = true;

        try {
            while (resultSet.next()) {
                if (resultSet.getInt("roleid") == 2 && resultSet.getInt("Counter") > 5) {

                    check2 = false;
                    break;
                }
                if (resultSet.getInt("roleid") == 2 && username.equals(resultSet.getString("username"))
                        && !password.equals(resultSet.getString("password"))) {

                    String counter2 = Integer.toString(resultSet.getInt("Counter") + 1);

                    String[] param2 = new String[] { counter2, username };

                    String sql2 = "UPDATE users SET counter = ? WHERE username = ?";
                    this.dbConnection.updateQuery(sql2, param2);

                }
            }
        } catch (SQLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return check2;

    }

    public boolean userExist(String username) {

        this.dbConnection = DBConnection.getInstance();
        String[] param = new String[] { username };
        String sql = "SELECT * FROM users WHERE username = ?";
        ResultSet resultSet = this.dbConnection.dataQueryGet(sql, param);
        boolean check2 = true;

        try {
            while (resultSet.next()) {
                // System.out.println(resultSet.getInt("roleid"));
                check2 = false;
                break;

            }
        } catch (SQLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return check2;

    }

    public boolean getLogin(String username, String password, HttpServletRequest req) {

        this.dbConnection = DBConnection.getInstance();

        String[] param = new String[] { username, password };
        String sql = "SELECT * FROM users WHERE username = ? AND password = ?";
        ResultSet resultSet = this.dbConnection.dataQueryGet(sql, param);
        boolean check = false;
        try {

            HttpSession session = req.getSession();
            // session will expire in 6000 seconds
            session.setMaxInactiveInterval(6000);
            while (resultSet.next()) {

                if (username.equals(resultSet.getString("username"))
                        && password.equals(resultSet.getString("password"))) {
                    session.setAttribute("id", resultSet.getString("id"));
                    session.setAttribute("roleid", resultSet.getString("roleid"));
                    session.setAttribute("firstname", resultSet.getString("firstname"));
                    session.setAttribute("lastname", resultSet.getString("lastname"));
                    session.setAttribute("idcard", resultSet.getString("idcard"));
                    session.setAttribute("tel", resultSet.getString("tel"));
                    session.setAttribute("email", resultSet.getString("email"));
                    session.setAttribute("address", resultSet.getString("address"));
                    session.setAttribute("line", resultSet.getString("line"));
                    session.setAttribute("storename", resultSet.getString("storename"));
                    session.setAttribute("username", resultSet.getString("username"));
                    session.setAttribute("password", resultSet.getString("password"));

                    // reset counter to 0 if Login success
                    String[] param2 = new String[] { username };
                    String sql2 = "UPDATE users SET counter = 0 WHERE username = ?";
                    this.dbConnection.updateQuery(sql2, param2);

                    check = true;
                    break;
                } else {
                    check = false;
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return check;

    }

    public User getProfile(String username) {

        this.dbConnection = DBConnection.getInstance();

        String[] param = new String[] { username };
        String sql = "SELECT * FROM users WHERE username = ? ";
        ResultSet resultSet = this.dbConnection.dataQueryGet(sql, param);
        try {

            while (resultSet.next()) {
                this.setUserid(resultSet.getInt("id"));
                this.setUsername(resultSet.getString("username"));
                this.setFirstname(resultSet.getString("firstname"));
                this.setLastname(resultSet.getString("lastname"));
                this.setIdcard(resultSet.getString("idcard"));
                this.setTel(resultSet.getString("tel"));
                this.setAddress(resultSet.getString("address"));
                this.setEmail(resultSet.getString("email"));
                this.setLine(resultSet.getString("line"));
                this.setStorename(resultSet.getString("storename"));

            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return this;

    }

    public void register(String username, String password, String firstname, String lastname, String idcard, String tel,
            String address, String email, String line, String storename) {

        this.dbConnection = DBConnection.getInstance();
        String[] param = new String[] { username, password, firstname, lastname, idcard, tel, address, email, line,
                storename };
        String sql = "INSERT INTO users (roleid,username,password,firstname,lastname,idcard,tel,address,email,line,storename,counter) VALUE (2,?,SHA2(?,256),?,?,?,?,?,?,?,?,0)";
        this.dbConnection.updateQuery(sql, param);

    }

    public void profile(String username, String password, String firstname, String lastname, String idcard, String tel,
            String address, String email, String line, String storename, HttpServletRequest req) {

        this.dbConnection = DBConnection.getInstance();
        String[] param = new String[] { username, password };
        String sql = "SELECT * FROM users WHERE username = ? AND password = ?";
        ResultSet resultSet = this.dbConnection.dataQueryGet(sql, param);

        HttpSession session = req.getSession();
        // session will expire in 600 seconds
        session.setMaxInactiveInterval(600);

        String[] param2 = new String[] { firstname, lastname, idcard, tel, address, email, line, storename,
                session.getAttribute("id").toString() };

        String sql2 = "UPDATE users SET firstname = ?, lastname = ?, idcard = ?, tel = ?, address = ?, email = ?, line = ?, storename = ? WHERE id = ?";
        this.dbConnection.updateQuery(sql2, param2);

    }

    public void resetPassword(String username, String password, HttpServletRequest req) {

        this.dbConnection = DBConnection.getInstance();
        String[] param = new String[] { username, password };
        String sql = "SELECT * FROM users WHERE username = ? AND password = ?";
        ResultSet resultSet = this.dbConnection.dataQueryGet(sql, param);

        HttpSession session = req.getSession();
        // session will expire in 600 seconds
        session.setMaxInactiveInterval(600);

        String[] param2 = new String[] { password, session.getAttribute("id").toString() };

        String sql2 = "UPDATE users SET password = SHA2(?,256) WHERE id = ?";
        this.dbConnection.updateQuery(sql2, param2);

    }

    public User getWinner(String keyword) {
        this.dbConnection = DBConnection.getInstance();
        String[] param = new String[] { keyword, keyword, keyword, keyword };
        String sql = "SELECT * FROM users WHERE username = ? or firstname = ? or lastname = ? or storename = ? ";
        ResultSet resultSet = this.dbConnection.dataQueryGet(sql, param);
        try {
            while (resultSet.next()) {
                this.setUserid(resultSet.getInt("id"));
                this.setUsername(resultSet.getString("username"));
                this.setFirstname(resultSet.getString("firstname"));
                this.setLastname(resultSet.getString("lastname"));
                this.setStorename(resultSet.getString("storename"));

            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return this;
    }

    public int getUserid() {
        return this.userid;
    }

    public void setUserid(int userid) {
        this.userid = userid;
    }

    public String getUsername() {
        return this.username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return this.password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getFirstname() {
        return this.firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getLastname() {
        return this.lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public String getRoleid() {
        return this.roleid;
    }

    public void setRoleid(String roleid) {
        this.roleid = roleid;
    }

    public String getIdcard() {
        return this.idcard;
    }

    public void setIdcard(String idcard) {
        this.idcard = idcard;
    }

    public String getTel() {
        return this.tel;
    }

    public void setTel(String tel) {
        this.tel = tel;
    }

    public String getAddress() {
        return this.address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getEmail() {
        return this.email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getLine() {
        return this.line;
    }

    public void setLine(String line) {
        this.line = line;
    }

    public String getStorename() {
        return this.storename;
    }

    public void setStorename(String storename) {
        this.storename = storename;
    }

    public DBConnection getDbConnection() {
        return this.dbConnection;
    }

    public void setDbConnection(DBConnection dbConnection) {
        this.dbConnection = dbConnection;
    }

    public int getCounter() {
        return this.counter;
    }

    public void setCounter(int counter) {
        this.counter = counter;
    }

    public User userid(int userid) {
        this.userid = userid;
        return this;
    }

    public User username(String username) {
        this.username = username;
        return this;
    }

    public User password(String password) {
        this.password = password;
        return this;
    }

    public User firstname(String firstname) {
        this.firstname = firstname;
        return this;
    }

    public User lastname(String lastname) {
        this.lastname = lastname;
        return this;
    }

    public User roleid(String roleid) {
        this.roleid = roleid;
        return this;
    }

    public User idcard(String idcard) {
        this.idcard = idcard;
        return this;
    }

    public User tel(String tel) {
        this.tel = tel;
        return this;
    }

    public User address(String address) {
        this.address = address;
        return this;
    }

    public User email(String email) {
        this.email = email;
        return this;
    }

    public User line(String line) {
        this.line = line;
        return this;
    }

    public User storename(String storename) {
        this.storename = storename;
        return this;
    }

    public User dbConnection(DBConnection dbConnection) {
        this.dbConnection = dbConnection;
        return this;
    }

    public User counter(int counter) {
        this.counter = counter;
        return this;
    }

}