package entity;

import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DecimalFormat;

import config.DBConnection;

public class ChkVehicle {
    private int id;
    private int vehicleid;
    private String ConditionReport;
    private int chk_AgreementNo;
    private int chk_auto_engine_no;
    private int chk_auto_body_no;
    private int condition_vehicle;
    private int wheel;
    private int frontbreak;
    private int backbreak;
    private int clutch;
    private int start;
    private int vehicle_key;
    private int ordersell;
    private BigDecimal pricestart;
    private DBConnection dbConnection;

    public void addChkVehicle(int vehicleid, String ConditionReport, int chk_AgreementNo, int chk_auto_engine_no,
            int chk_auto_body_no, int condition_vehicle, int wheel, int frontbreak, int backbreak, int clutch,
            int start, int vehicle_key, int ordersell, BigDecimal pricestart) {

        this.dbConnection = DBConnection.getInstance();
        String[] param = new String[] { Integer.toString(vehicleid), ConditionReport, Integer.toString(chk_AgreementNo),
                Integer.toString(chk_auto_engine_no), Integer.toString(chk_auto_body_no),
                Integer.toString(condition_vehicle), Integer.toString(wheel), Integer.toString(frontbreak),
                Integer.toString(backbreak), Integer.toString(clutch), Integer.toString(start),
                Integer.toString(vehicle_key), Integer.toString(ordersell), pricestart.toString() };
        String sql = "INSERT INTO chkvehicle (vehicleid,ConditionReport,chk_AgreementNo,chk_auto_engine_no,chk_auto_body_no,condition_vehicle,wheel,frontbreak,backbreak,clutch,start,vehicle_key,ordersell,pricestart) VALUE (?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
        this.dbConnection.updateQuery(sql, param);
    }

    public void updateChkVehicle(int id, int vehicleid, String ConditionReport, int chk_AgreementNo,
            int chk_auto_engine_no, int chk_auto_body_no, int condition_vehicle, int wheel, int frontbreak,
            int backbreak, int clutch, int start, int vehicle_key, int ordersell, BigDecimal pricestart) {
        this.dbConnection = DBConnection.getInstance();
        String[] param = new String[] { Integer.toString(vehicleid), ConditionReport, Integer.toString(chk_AgreementNo),
                Integer.toString(chk_auto_engine_no), Integer.toString(chk_auto_body_no),
                Integer.toString(condition_vehicle), Integer.toString(wheel), Integer.toString(frontbreak),
                Integer.toString(backbreak), Integer.toString(clutch), Integer.toString(start),
                Integer.toString(vehicle_key), Integer.toString(ordersell), pricestart.toString(),
                Integer.toString(id) };
        String sql = "UPDATE chkvehicle set vehicleid = ?, ConditionReport = ?,chk_AgreementNo = ?,chk_auto_engine_no = ?,chk_auto_body_no = ?,condition_vehicle = ?,wheel = ?,frontbreak = ?,backbreak = ?,clutch = ?,start = ?,vehicle_key = ?,ordersell = ?,pricestart = ? WHERE id = ? ";
        this.dbConnection.updateQuery(sql, param);
    }

    public ChkVehicle getChkVehicleByID(String id) {

        this.dbConnection = DBConnection.getInstance();

        String[] param = new String[] { id };
        String sql = "SELECT * FROM chkvehicle WHERE vehicleid = ?";
        ResultSet resultSet = this.dbConnection.dataQueryGet(sql, param);
        try {
            while (resultSet.next()) {
                ChkVehicle chkVehicle = new ChkVehicle();
                this.setId(resultSet.getInt("id"));
                this.setVehicleid(resultSet.getInt("vehicleid"));
                this.setConditionReport(resultSet.getString("ConditionReport"));
                this.setChk_AgreementNo(resultSet.getInt("chk_AgreementNo"));
                this.setChk_auto_engine_no(resultSet.getInt("chk_auto_engine_no"));
                this.setChk_auto_body_no(resultSet.getInt("chk_auto_body_no"));
                this.setCondition_vehicle(resultSet.getInt("condition_vehicle"));
                this.setWheel(resultSet.getInt("wheel"));
                this.setFrontbreak(resultSet.getInt("frontbreak"));
                this.setBackbreak(resultSet.getInt("backbreak"));
                this.setClutch(resultSet.getInt("clutch"));
                this.setStart(resultSet.getInt("start"));
                this.setVehicle_key(resultSet.getInt("vehicle_key"));
                this.setOrdersell(resultSet.getInt("ordersell"));
                this.setPricestart(resultSet.getBigDecimal("pricestart"));

            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return this;
    }

    public ChkVehicle() {
    }

    public ChkVehicle(int id, int vehicleid, String ConditionReport, int chk_AgreementNo, int chk_auto_engine_no,
            int chk_auto_body_no, int condition_vehicle, int wheel, int frontbreak, int backbreak, int clutch,
            int start, int vehicle_key, int ordersell, BigDecimal pricestart, DBConnection dbConnection) {
        this.id = id;
        this.vehicleid = vehicleid;
        this.ConditionReport = ConditionReport;
        this.chk_AgreementNo = chk_AgreementNo;
        this.chk_auto_engine_no = chk_auto_engine_no;
        this.chk_auto_body_no = chk_auto_body_no;
        this.condition_vehicle = condition_vehicle;
        this.wheel = wheel;
        this.frontbreak = frontbreak;
        this.backbreak = backbreak;
        this.clutch = clutch;
        this.start = start;
        this.vehicle_key = vehicle_key;
        this.ordersell = ordersell;
        this.pricestart = pricestart;
        this.dbConnection = dbConnection;
    }

    public int getId() {
        return this.id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getVehicleid() {
        return this.vehicleid;
    }

    public void setVehicleid(int vehicleid) {
        this.vehicleid = vehicleid;
    }

    public String getConditionReport() {
        return this.ConditionReport;
    }

    public void setConditionReport(String ConditionReport) {
        this.ConditionReport = ConditionReport;
    }

    public int getChk_AgreementNo() {
        return this.chk_AgreementNo;
    }

    public void setChk_AgreementNo(int chk_AgreementNo) {
        this.chk_AgreementNo = chk_AgreementNo;
    }

    public int getChk_auto_engine_no() {
        return this.chk_auto_engine_no;
    }

    public void setChk_auto_engine_no(int chk_auto_engine_no) {
        this.chk_auto_engine_no = chk_auto_engine_no;
    }

    public int getChk_auto_body_no() {
        return this.chk_auto_body_no;
    }

    public void setChk_auto_body_no(int chk_auto_body_no) {
        this.chk_auto_body_no = chk_auto_body_no;
    }

    public int getCondition_vehicle() {
        return this.condition_vehicle;
    }

    public void setCondition_vehicle(int condition_vehicle) {
        this.condition_vehicle = condition_vehicle;
    }

    public int getWheel() {
        return this.wheel;
    }

    public void setWheel(int wheel) {
        this.wheel = wheel;
    }

    public int getFrontbreak() {
        return this.frontbreak;
    }

    public void setFrontbreak(int frontbreak) {
        this.frontbreak = frontbreak;
    }

    public int getBackbreak() {
        return this.backbreak;
    }

    public void setBackbreak(int backbreak) {
        this.backbreak = backbreak;
    }

    public int getClutch() {
        return this.clutch;
    }

    public void setClutch(int clutch) {
        this.clutch = clutch;
    }

    public int getStart() {
        return this.start;
    }

    public void setStart(int start) {
        this.start = start;
    }

    public int getVehicle_key() {
        return this.vehicle_key;
    }

    public void setVehicle_key(int vehicle_key) {
        this.vehicle_key = vehicle_key;
    }

    public int getOrdersell() {
        return this.ordersell;
    }

    public void setOrdersell(int ordersell) {
        this.ordersell = ordersell;
    }

    public BigDecimal getPricestart() {
        return this.pricestart;
    }

    public void setPricestart(BigDecimal pricestart) {
        this.pricestart = pricestart;
    }

    public DBConnection getDbConnection() {
        return this.dbConnection;
    }

    public void setDbConnection(DBConnection dbConnection) {
        this.dbConnection = dbConnection;
    }

    public ChkVehicle id(int id) {
        this.id = id;
        return this;
    }

    public ChkVehicle vehicleid(int vehicleid) {
        this.vehicleid = vehicleid;
        return this;
    }

    public ChkVehicle ConditionReport(String ConditionReport) {
        this.ConditionReport = ConditionReport;
        return this;
    }

    public ChkVehicle chk_AgreementNo(int chk_AgreementNo) {
        this.chk_AgreementNo = chk_AgreementNo;
        return this;
    }

    public ChkVehicle chk_auto_engine_no(int chk_auto_engine_no) {
        this.chk_auto_engine_no = chk_auto_engine_no;
        return this;
    }

    public ChkVehicle chk_auto_body_no(int chk_auto_body_no) {
        this.chk_auto_body_no = chk_auto_body_no;
        return this;
    }

    public ChkVehicle condition_vehicle(int condition_vehicle) {
        this.condition_vehicle = condition_vehicle;
        return this;
    }

    public ChkVehicle wheel(int wheel) {
        this.wheel = wheel;
        return this;
    }

    public ChkVehicle frontbreak(int frontbreak) {
        this.frontbreak = frontbreak;
        return this;
    }

    public ChkVehicle backbreak(int backbreak) {
        this.backbreak = backbreak;
        return this;
    }

    public ChkVehicle clutch(int clutch) {
        this.clutch = clutch;
        return this;
    }

    public ChkVehicle start(int start) {
        this.start = start;
        return this;
    }

    public ChkVehicle vehicle_key(int vehicle_key) {
        this.vehicle_key = vehicle_key;
        return this;
    }

    public ChkVehicle ordersell(int ordersell) {
        this.ordersell = ordersell;
        return this;
    }

    public ChkVehicle pricestart(BigDecimal pricestart) {
        this.pricestart = pricestart;
        return this;
    }

    public ChkVehicle dbConnection(DBConnection dbConnection) {
        this.dbConnection = dbConnection;
        return this;
    }

}