package config;

import com.mysql.jdbc.Driver;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

public class DBConnection {

    private Connection connection;
    private static DBConnection instance = null;
    private ResultSet resultSet;

    public static DBConnection getInstance() {
        if (instance == null)
            instance = new DBConnection("localhost", "webapplication", "P@ssw0rd", "mntl");

        // Database

        return instance;
    }

    public DBConnection(String host, String username, String password, String database) { // Custom Database
        try {
            DriverManager.registerDriver(new Driver());
            connection = DriverManager.getConnection("jdbc:mysql://" + host + "/" + database
                    + "?useUnicode=yes&characterEncoding=UTF-8&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC",
                    username, password);
        } catch (SQLException ex) {
            System.out.println("Connection error: " + ex.toString());
        }
    }

    public ResultSet dataQueryGet(String sql, String[] param) {
        try {
            PreparedStatement statement = connection.prepareStatement(sql);
            for (int i = 0; i < param.length; i++) {
                statement.setString(i + 1, param[i]);

            }

            this.resultSet = statement.executeQuery();

            // statement.close();
        } catch (SQLException ex) {
            System.out.println("Query Error : " + sql);
            System.out.println("Exception: " + ex.toString());
        }
        return this.resultSet;
    }

    public void updateQuery(String sql, String[] bindValues) {

        try {
            PreparedStatement statement = connection.prepareStatement(sql);
            for (int i = 0; i < bindValues.length; i++) {
                statement.setString(i + 1, bindValues[i]);
            }

            statement.executeUpdate();

            // statement.close();
        } catch (SQLException ex) {
            System.out.println("Query Error : " + sql);
            System.out.println("Exception: " + ex.toString());
        }
    }

    public void closeConnection() {
        try {
            connection.close();
        } catch (SQLException ex) {
            System.out.println("Cannot Close Connection" + ex.toString());
        }
    }
}